<?php

return [
    'post_type' => [
        POST_TYPE_TEXT => 'Bài viết',
        POST_TYPE_VIDEO => 'Video',
    ],
    'post_status' => [
        POST_ACTIVE => 'Đang hiển thị',
        POST_DISABLED => 'Đã gỡ',
    ]
];
