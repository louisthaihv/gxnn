<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */
use App\Models\Post;
use Illuminate\Support\Str;
use Faker\Generator as Faker;
use Carbon\Carbon;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(Post::class, function (Faker $faker) {
    $title = $faker->name;
    $categoryId = rand(1, 10);
    $images = [
        1 => [
            'images/post/bg_1.jpg',
            'images/post/bg_2.jpg',
            'images/post/bg_3.jpg',
            'images/post/bg_4.jpg',
        ],
        2 => [
            'images/post/about.jpg'
        ],
        3 => [
            'images/post/sermon-1.jpg',
            'images/post/sermon-2.jpg',
            'images/post/sermon-3.jpg',
            'images/post/sermon-4.jpg',
            'images/post/sermon-5.jpg',
            'images/post/sermon-6.jpg',
        ],
        4 => [
            'images/post/event-1.jpg',
            'images/post/event-2.jpg',
            'images/post/event-3.jpg',
            'images/post/event-4.jpg',
            'images/post/event-5.jpg',
            'images/post/event-6.jpg',
        ],
        5 => [
            'images/post/image_1.jpg',
            'images/post/image_2.jpg',
            'images/post/image_3.jpg',
            'images/post/image_4.jpg',
            'images/post/image_5.jpg',
            'images/post/image_6.jpg',
        ],
        6 => [
            'images/post/linhmuc.jpg'
        ],
        7 => [
            ''
        ],
        9 => [
            'images/post/gallery-1.jpg',
            'images/post/gallery-2.jpg',
            'images/post/gallery-3.jpg',
            'images/post/gallery-4.jpg',
        ],
        8 => [
            'images/post/activity1.png',
            'images/post/activity2.png',
            'images/post/activity3.png',
            'images/post/activity4.png',
            'images/post/activity5.png'
        ],
        10 => [
            'images/post/bg_1.jpg',
        ]
    ];
    return [
        'user_id' => 1,
        'category_id' => $categoryId,
        'title' => $title,
        'slug' => Str::slug($title),
        'description' => $categoryId == 6 || $categoryId == 1 ? $faker->name : $faker->name. $faker->name . $faker->name . $faker->name,
        'content' => $categoryId == 6 ? 'Cha xứ cựu' : $faker->text,
        'image' => $images[$categoryId][array_rand($images[$categoryId])],
        'source' => 'https://www.youtube.com/watch?v=1GTqGVHer4c',
        'published_at' => Carbon::today(),
        'expired_at' => Carbon::yesterday(),
        'updated_at' => date('Y-m-d H:i:s'),
        'created_at' => date('Y-m-d H:i:s')
    ];
});
