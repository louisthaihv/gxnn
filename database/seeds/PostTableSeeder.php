<?php

use Illuminate\Database\Seeder;
use App\Models\Post;

class PostTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Post::truncate();
        factory(Post::class, 50)->create();
        $posts = [
            ['title' => 'GIÁO XỨ NHÂN NGHĨA',
            'description' =>'',
            'content' => 'Khởi công thì khó khăn, kết thúc thì tốt đẹp; bắt đầu thì lo âu, hoàn thành thì hoan hỷ vui mừng”, cha xứ Gioan Baotixita Bùi Văn Hân đã phát biểu như vậy trong lễ khánh thành ngôi thánh đường giáo xứ diễn ra vào lúc 9g30 ngày 01-01-2015. Lời phát biểu ấy cho thấy công trình nhà thờ quả là kỳ diệu. Điều kỳ diệu được dệt nên bởi hồng ân Chúa đã tuôn đổ dồi dào trên mảnh đất Nhân Nghĩa này. Với cảm nhận ấy, không chỉ người dân nơi đây mà tất cả những ai đến tham dự sự kiện đặc biệt này đều hòa chung với tâm tình mừng vui của tác giả Thánh vịnh mà cất cao lời ca ngợi: “Đây là ngày Chúa đã làm ra, nào ta hãy vui mừng hoan hỷ” (Tv 118,24).',
            'image' => 'images/post/01-Giao-Phan-HaiPhong-NhanNghia-00-1.jpg',
            'user_id' => 1,
            'category_id' => ABOUT_ID,
            'slug' => 'giáo-xứ-nhân-nghĩa',
            'updated_at' => date('Y-m-d H:i:s'),
            'created_at' => date('Y-m-d H:i:s')],
            [
                'title' => 'Chúa Nói',
                'description' =>'John 3:16 KJV',
                'content' => '16 Vì Thiên Chúa yêu thế gian, đến nỗi Người đã ban cho Người Con duy nhất của Người, rằng bất cứ ai tin vào Người không nên bị diệt vong, nhưng có được sự sống đời đời.',
                'image' => 'images/post/bg_2.jpg',
                'user_id' => 1,
                'category_id' => DAILY_VERSE,
                'slug' => 'chúa-nói',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'Lịch Sử',
                'description' =>'Lịch Sử Giáo Sứ',
                'content' => 'Lịch sử giáo sứ viết vào đây',
                'image' => 'images/post/bg_2.jpg',
                'user_id' => 1,
                'category_id' => THONG_TIN_ID,
                'slug' => 'lịch-sử',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'Nhân Sự',
                'description' =>'Nhân Sự Giáo Sứ',
                'content' => 'Nhân sự của giáo sứ viết vào đây',
                'image' => 'images/post/bg_2.jpg',
                'user_id' => 1,
                'category_id' => THONG_TIN_ID,
                'slug' => 'nhân-sự',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'Hội Đoàn',
                'description' =>'Các Hội Đoàn Của Giáo Sứ',
                'content' => 'Thông tin hội đoàn viết vào đây',
                'image' => 'images/post/bg_2.jpg',
                'user_id' => 1,
                'category_id' => THONG_TIN_ID,
                'slug' => 'hội-đoàn',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'Lễ Thứ Năm',
                'description' =>'19:30 ~ 20:30',
                'content' => 'Lễ cưới được tổ chức vào ngày này',
                'image' => 'images/post/bg_2.jpg',
                'user_id' => 1,
                'category_id' => THANH_LE_ID,
                'slug' => 'lễ-thứ-năm',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'Lễ Thứ Bảy',
                'description' =>'19:30 ~ 20:30',
                'content' => 'Lễ Chúa Nhật',
                'image' => 'images/post/bg_2.jpg',
                'user_id' => 1,
                'category_id' => THANH_LE_ID,
                'slug' => 'lễ-thứ-bảy',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
            [
                'title' => 'Lễ Chủ Nhật',
                'description' =>'15:30~16:30',
                'content' => 'Lễ Thiếu Nhi',
                'image' => 'images/post/bg_2.jpg',
                'user_id' => 1,
                'category_id' => THANH_LE_ID,
                'slug' => 'lễ-chủ-nhật',
                'updated_at' => date('Y-m-d H:i:s'),
                'created_at' => date('Y-m-d H:i:s')
            ],
        ];
        App\Models\Post::insert($posts);
    }
}
