<?php

use Illuminate\Database\Seeder;
use App\Models\Category;

class CategoryTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        App\Models\Category::truncate();
        $categories = [
            [
                'title' => 'Trang Chủ',
                'type' => CATEGORY_MENU,
                'description' => null,
                'content' => null,
                'image' => null
            ],
            [
                'title' => 'About',
                'type' => CATEGORY_MENU,
                'description' => null,
                'content' => null,
                'image' => null
            ],
            [
                'title' => 'Bài Giảng',
                'type' => CATEGORY_MENU,
                'description' => 'Bài Giảng Mới Nhất',
                'content' => 'Cùng nhau suy ngẫm và thực hiện theo lời Cha, lời Chúa dạy',
                'image' => null
            ],
            [
                'title' => 'Sự Kiện',
                'type' => CATEGORY_MENU,
                'description' => 'Sự Kiện Sắp Diễn Ra',
                'content' => null,
                'image' => 'images/categories/event.png'
            ],
            [
                'title' => 'Diễn Đàn',
                'type' => CATEGORY_MENU,
                'description' => 'Bài Viết Mới Nhất',
                'content' => 'Nơi chia sẻ, bàn luận, suy niệm về lời Chúa. Hãy cùng nhau xây dựng đời sống kito giáo.',
                'image' => null
            ],
            [
                'title' => 'Linh Mục',
                'type' => CATEGORY_MENU,
                'description' => 'Danh Sách Cha Xứ',
                'content' => 'Cầu mong bình an, sức khỏe cho các ngài, để các ngài công tác sốt sắng và luôn hoàn thành tốt mọi nhiệm vụ được giao',
                'image' => null
            ],
            [
                'title' => 'Liên Hệ',
                'type' => CATEGORY_MENU,
                'description' => null,
                'content' => null,
                'image' => null
            ],
            [
                'title' => 'Hoạt Động - Nhiệm Vụ',
                'type' => NONE_CATEGORY_MENU,
                'description' => 'Cùng Nhau Cầu Nguyện',
                'content' => 'Loan Báo Tin Mừng - Kết nối yêu Thương - Sống tình Huynh Đệ - Làm Việc Bác Ái - Chia Sẻ những bài viết, hình ảnh liên quan đến giáo hội công giáo, giáo xứ và giáo phận',
                'image' => null
            ],
            [
                'title' => 'GALLERY',
                'type' => NONE_CATEGORY_MENU,
                'description' => 'Hình ảnh về giáo xứ',
                'content' => null,
                'image' => null
            ],
            [
                'title' => 'Daily-Verse',
                'type' => NONE_CATEGORY_MENU,
                'description' => 'Hình ảnh về giáo xứ',
                'content' => null,
                'image' => null
            ],
            [
                'title' => 'Thông Tin',
                'type' => NONE_CATEGORY_MENU,
                'description' => '',
                'content' => null,
                'image' => null
            ],
            [
                'title' => 'Thánh Lễ',
                'type' => NONE_CATEGORY_MENU,
                'description' => '',
                'content' => null,
                'image' => null
            ]
        ];

        Category::insert($categories);
    }
}
