<?php

use Illuminate\Database\Seeder;
use Illuminate\Database\Eloquent\Model;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        DB::statement('SET FOREIGN_KEY_CHECKS=0');
        Model::unguard();
        $seeders = [
            UsersTableSeeder::class,
            CategoryTableSeeder::class,
            PostTableSeeder::class
        ];

        foreach ($seeders as $seeder) {
            $this->call($seeder);
        }
        Model::reguard();
        DB::statement('SET FOREIGN_KEY_CHECKS=1');
    }
}
