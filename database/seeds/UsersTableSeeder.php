<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        App\Models\User::truncate();
        $admin = [
            'name' => 'Admin',
            'email' => 'admin@admin.com',
            'password' => bcrypt('admin'),
            'role_id' => 4,
            'avatar' => 'https://lorempixel.com/150/150/?93243',
            'phone_number' => '+84989454902',
            'description' => 'Description'
        ];

        App\Models\User::insert([
            $admin
        ]);
        factory(App\Models\User::class, 50)->create();

    }
}
