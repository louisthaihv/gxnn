<?php

const POST_IMAGE_PATH = 'images/post';
const POST_TYPE_VIDEO = 2;
const POST_TYPE_TEXT = 1;
const POST_ACTIVE = 1;
const POST_DISABLED = 0;
const POST_BY_CATEGORY_LIMIT = 10;
const PAGINATE = 10;