<?php

const CATEGORY_IMAGE_PATH = 'images/categories';
const CATEGORY_MENU = 0;
const NONE_CATEGORY_MENU = 1;
const TRANG_CHU_ID = 1;
const ABOUT_ID = 2;
const BAI_GIANG_ID = 3;
const SU_KIEN_ID = 4;
const DIEN_DAN_ID = 5;
const LINH_MUC_ID = 6;
const LIEN_HE_ID = 7;
const HOAT_DONG_ID = 8;
const GALLERY_ID = 9;
const DAILY_VERSE = 10;
const THONG_TIN_ID = 11;
const THANH_LE_ID = 12;