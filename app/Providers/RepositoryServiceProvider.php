<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;

use App\Repositories\User\UserRepository;
use App\Repositories\User\IUserRepository;
use App\Repositories\Post\PostRepository;
use App\Repositories\Post\IPostRepository;
use App\Repositories\Category\CategoryRepository;
use App\Repositories\Category\ICategoryRepository;

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {


        foreach ([
                IUserRepository::class => UserRepository::class,
                IPostRepository::class => PostRepository::class,
                ICategoryRepository::class => CategoryRepository::class,

            ] as $interface => $concrete) {

            app()->bind($interface, $concrete);
        }
    }
}
