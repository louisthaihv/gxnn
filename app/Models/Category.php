<?php

namespace App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use SoftDeletes;

    protected $guarded = ['id'];

    public function posts()
    {
        return $this->hasMany(Post::class, 'category_id', 'id');
    }

    public function getImageUrl()
    {
        return asset("storage/{$this->image}");
    }
}
