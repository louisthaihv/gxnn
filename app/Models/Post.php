<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Support\Str;

class Post extends Model
{
    use SoftDeletes;
    protected $guarded = ['id'];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function category()
    {
        return $this->belongsTo(Category::class, 'category_id', 'id')->withTrashed();
    }
    public function getImageUrl()
    {
        return asset("storage/{$this->image}");
    }

    public function getLimitContent($limit = 100, $end = '...')
    {
        return Str::limit($this->content, $limit, $end);
    }

    public function getPublished()
    {
        $time = strtotime($this->published_at);
        return $this->published_at ? date('d/m/y', $time) : '?';
    }
    public function getExpired()
    {
        $time = strtotime($this->expired_at);
        return $this->expired_at ? date('d/m/y', $time) : '?';
    }

    public function getDay()
    {
        return $this->created_at->day;
    }

    public function getYear()
    {
        return $this->created_at->year;
    }

    public function getMonth()
    {
        return $this->created_at->month;
    }
}
