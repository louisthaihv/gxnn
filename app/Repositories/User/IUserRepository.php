<?php

namespace App\Repositories\User;


use App\Models\User;

interface IUserRepository {
    public function pluckAdmin();

    public function insertUserIncome(User $user, $data);

    public function getUserCanHasOrder();


    public function getIncomeByUserId($filters);

    public function getTotalOrdersByUserId($filters);

    public function getRevenueByUserId($filters);
}
