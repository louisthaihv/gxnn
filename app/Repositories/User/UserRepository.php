<?php

namespace App\Repositories\User;


use App\Models\User;
use App\Models\UserIncome;
use App\Repositories\BaseRepository;

class UserRepository extends BaseRepository implements IUserRepository
{
    public function model() {
        return User::class;
    }

    public function pluckAdmin() {

        return $this->model->isAdmin()->pluck('name', 'id')->toArray();
    }

    public function insertUserIncome(User $user, $data) {
        $userIncome = new UserIncome($data);

        return $user->userIncomes()->save($userIncome);

    }

    public function getUserCanHasOrder() {
        return $this->model->whereIn('role_id', [ADMIN, TEACHER, SALE])->pluck('name', 'id')->toArray();
    }

    public function getIncomeByUserId($filters) {
        $query = $this->find($filters['user_id'])->userIncomes();

        $query = $this->filterByDate($filters, $query);

        return $query->distinct('order_id')->sum('income');
    }

    public function getTotalOrdersByUserId($filters) {
        $query = $this->find($filters['user_id'])->userIncomes();

        $query = $this->filterByDate($filters, $query);

        return $query->distinct('order_id')->count('order_id');
    }

    public function getRevenueByUserId($filters) {
        $query = $this->find($filters['user_id'])
            ->userIncomes()
            ->select(
                $this->raw('YEAR(created_at) as by_year'),
                $this->raw('MONTH(created_at) as by_month'),
                $this->raw('SUM(income) as total_price')
            );

        $query = $this->filterByDate($filters, $query);

        return $query
            ->groupBy('by_year')
            ->groupBy('by_month')
            ->orderBy('by_year', 'ASC')
            ->orderBy('by_month', 'ASC')
            ->get();
    }

}
