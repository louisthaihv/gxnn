<?php

namespace App\Repositories\Post;


use App\Models\Post;
use App\Repositories\BaseRepository;

class PostRepository extends BaseRepository implements IPostRepository {
    public function model() {
        return Post::class;
    }

    public function findByCategoryId($categoryId, $limit = POST_BY_CATEGORY_LIMIT)
    {
        return $this->model->where('active', 1)
            ->where('category_id', $categoryId)
            ->orderBy('updated_at', 'desc')
            ->take($limit)
            ->get();
    }

    public function randomByCategoryId($categoryId, $limit = POST_BY_CATEGORY_LIMIT)
    {
        return $this->model->where('active', 1)
            ->where('category_id', $categoryId)
            ->where('type', 1)
            ->inRandomOrder()->limit($limit)->get();
    }

    public function getFirstByCategoryAndType($categoryId, $type = 1)
    {
        return $this->model->where('active', 1)
            ->where('category_id', $categoryId)
            ->where('type', $type)
            ->first();
    }

    public function findPostBySlug($slug, $relations = [])
    {
        $post = $this->model->where('slug', $slug);
        return empty($relations) ? $post->get()->first() : $this->model->with($relations)->get()->first(); 
    }

    public function searchPost($search, $limit = 10)
    {
        return $this->model->where('title', 'like', '%'.$search.'%')
            ->orWhere('description', 'like', '%'.$search.'%')
            ->orWhere('content', 'like', '%'.$search.'%')->paginate($limit);
    }

    public function paginatePostByCategoryId($categoryId, $limit = 10)
    {
        return $this->model->where('category_id', $categoryId)->paginate($limit);
    }
}
