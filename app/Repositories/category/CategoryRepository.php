<?php

namespace App\Repositories\Category;


use App\Models\Category;
use App\Repositories\BaseRepository;

/**
 * Undocumented class
 */
class CategoryRepository extends BaseRepository implements ICategoryRepository
{
    /**
     * Undocumented function
     *
     * @return void
     */
    public function model()
    {
        return Category::class;
    }

    public function getAllPostById($id, $limit = 10)
    {
        return $this->model->where('id', $id)->with(['posts' => function($q) use($limit){
            $q->orderBy('updated_at', 'desc')->orderBy('id', 'desc')->paginate($limit);
        }])->first();
    }

    public function getMenu()
    {
        return $this->model->where('type', CATEGORY_MENU)->select('id', 'title')->get();
    }

    public function getAllCategoryWithCountPost()
    {
        return $this->model->withCount('posts')->get();
    }
}
