<?php

namespace App\Services;

use App\Repositories\Category\ICategoryRepository;
use Illuminate\Http\Request;

class CategoryService extends BaseService
{

    /**
     * Undocumented function
     *
     * @param ICategoryRepository $categoryRepository
     */
    public function __construct(ICategoryRepository $categoryRepository)
    {
        $this->repository = $categoryRepository;
    }

    /**
     * Undocumented function
     *
     * @param array $filters
     * @return void
     */
    public function getAsJson($filters = [])
    {
        return $this->repository->getAsJson($filters);
    }

    public function saveCategory(Request $request)
    {
        $path = $request->file('image')->store(CATEGORY_IMAGE_PATH, 'public');
        $categoryData = $request->only('title', 'content', 'description');
        $categoryData['image'] = $path;
        if ($saved = $this->insert($categoryData)) {
            return [
                'message' => 'Thêm mới thành công',
                'code' => 200,
                'data' => [
                    'url' => route('categories.index')
                ]
            ];
        }

        return [
            'message' => 'Thêm mới thất bại',
            'code' => 500,
            'data' => [
                'url' => route('categories.index')
            ]
        ];
    }
    
    public function findAndUpdateCategory($id, Request $request)
    {
        $categoryData = $request->only('title', 'content', 'description');
        if ($request->hasFile('image')) {
            $path = $request->file('image')[0]->store(CATEGORY_IMAGE_PATH, 'public');
            $categoryData['image'] = $path;
        }
        if (parent::findAndUpdate($id, $categoryData)) {
            if ($request->ajax()) {
                return [
                    'message' => 'Cập nhật thành công',
                    'code' => 200,
                    'data' => [
                        'url' => route('categories.edit', $id)
                    ]
                ];
            }
            return redirect()->back()->with('success', 'Cập nhật thông tin thành công');
        }
        if ($request->ajax()) {
            return [
                'message' => 'Cập nhật thất bại',
                'code' => 500,
                'data' => [
                    'url' => route('categories.edit', $id)
                ]
            ];
        }
        return redirect()->back()->with('error', 'Cập nhật thông tin thất bại');
    }

    public function getAllCategorySelectBox($emptyOption = true)
    {
        $all = $this->repository->pluckAll('title');
        if ($emptyOption) {
            array_unshift($all, 'Tất cả');
        }
        return $all;
    }

    public function getAllPostById($id, $limit = 10)
    {
        return $this->repository->getAllPostById($id, $limit);
    }

    public function getMenu()
    {
        return $this->repository->getMenu()->toArray();
    }

    public function getAllCategoryWithCountPost()
    {
        return $this->repository->getAllCategoryWithCountPost();
    }
}