<?php
namespace App\Services;

use Illuminate\Support\Facades\Mail;
use App\Mail\Contact;

class HomeService
{
    private $postService;
    private $categoryService;

    public function __construct(PostService $postService, CategoryService $categoryService)
    {
        $this->postService = $postService;
        $this->categoryService = $categoryService;
    }

    public function getHomeData()
    {
        $homePost = $this->categoryService->getAllPostById(TRANG_CHU_ID, 6);
        $about = $this->categoryService->getAllPostById(ABOUT_ID, 1);
        $sermon = $this->categoryService->getAllPostById(BAI_GIANG_ID, 6);
        $event = $this->categoryService->getAllPostById(SU_KIEN_ID, 6);
        $blog = $this->categoryService->getAllPostById(DIEN_DAN_ID, 6);
        $pastor = $this->categoryService->getAllPostById(LINH_MUC_ID, 6);
        $contact = $this->categoryService->getAllPostById(LIEN_HE_ID, 6);
        $activity = $this->categoryService->getAllPostById(HOAT_DONG_ID, 6);
        $gallery = $this->categoryService->getAllPostById(GALLERY_ID, 6);
        $dailyVerse = $this->categoryService->getAllPostById(DAILY_VERSE, 1);
        $viewData = [
            'home_slide' => $homePost,
            'about' => $about->posts->first(),
            'sermon' =>$sermon,
            'event' => $event,
            'blog' => $blog,
            'pastor' => $pastor,
            'contact' => $contact,
            'activity' => $activity,
            'gallery' => $gallery,
            'dailyVerse' => $dailyVerse->posts->first(),
        ];
        return $viewData;
    }

    public function getSinglePostData($slug)
    {
        $post = $this->postService->findPostBySlug($slug, ['category']);
        $categories = $this->categoryService->getAllCategoryWithCountPost();
        $relatePost = $this->categoryService->getAllPostById($post->category->id, 5);
        $viewData = [
            'post' => $post,
            'categories' => $categories,
            'relatePosts' => $relatePost
        ];
        return $viewData;
    }

    public function searchPost($search)
    {
        return $this->postService->searchPost($search);
    }

    public function getCategoryAndPagingPost($categoryId)
    {
        $category = $this->categoryService->find($categoryId);
        $posts = $this->postService->paginatePostByCategoryId($categoryId);
        return [
            'category' => $category,
            'posts' => $posts
        ];
    }

    public function sendMail($data)
    {
        try {
            Mail::to(env('MAIL_ADMIN'))->send(new Contact($data));
            return [
                'message' => 'Gửi email thành công!',
                'code' => 200
            ];
        } catch(Exception $ex) {
            \Log::error('Something went wrong when send email ' . __METHOD__ . ' with : ' . $ex->getMessage());
            return [
                'message' => 'Gửi email thất bại!',
                'code' => 500
            ];
        }
    }

}