<?php

namespace App\Services;


use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;
//use Intervention\Image\Image as Image;

class UploadFileService {
    protected $uploadedFile;
    protected $uploadedFolder;

    public function __construct(UploadedFile $uploadedFile, $uploadedFolder) {
        $this->uploadedFile = $uploadedFile;

        $this->uploadedFolder = 'uploads/' . $uploadedFolder;
    }

    public function upload($name = '') {
        $fileName = implode('.', [$this->generateFileName($name), $this->getExtension()]);
        try {
            $this->uploadedFile->move(public_path($this->uploadedFolder), $fileName);

            return [
                'content_path' => $this->uploadedFolder . '/' . $fileName,
                'file_type' => head(explode('/', $this->getMimeType()))
            ];
        } catch (\Exception $exception) {
            Log::error('something went wrong when upload file ' . $fileName . ' with exception ' . $exception->getMessage());
            return false;
        }
    }

    public function getOriginalName() {
        return $this->uploadedFile->getClientOriginalName();
    }

    private function getExtension() {
        return $this->uploadedFile->getClientOriginalExtension();
    }

    private function getMimeType() {
        return $this->uploadedFile->getClientMimeType();
    }

    private function generateFileName($title = '') {

        return !empty($title) ? Str::slug($title) . '-' . Str::random(4) : Str::random(8);
    }

//    private function resize($filePath) {
//        try {
//           Image::make($filePath)->widen(200);
//
//            return $filePath;
//        } catch (\Exception $exception) {
//            Log::error('something went wrong when resize file ' . $fileName . ' with exception ' . $exception->getMessage());
//            return $filePath;
//        }
//    }
}
