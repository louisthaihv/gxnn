<?php

namespace App\Services;

use Exception;
use App\Repositories\User\IUserRepository;
use App\Http\Requests\Admin\UserRequest;
class UserService extends BaseService {


    public function __construct(IUserRepository $userRepository) {
        $this->repository = $userRepository;
    }

    public function getAsJson($filters) {
        return $this->repository->getAsJson($filters);
    }

    public function findAndUpdate($id, $data = []) {
        try {
            if (!$instance = $this->find($id)) {
                return false;
            }
            $data['password'] = !empty($data['password']) ? bcrypt($data['password']) : $instance->password;
            if ($instance instanceof $this->repository->model) {
                $instance->fill($data)->save();
            }

            return $instance;
        } catch (Exception $exception) {
            \Log::error('Something went wrong when update ' . __METHOD__ . ' with : ' . $exception->getMessage());
            return false;
        }
    }

    public function pluckAdmin() {
        return $this->repository->pluckAdmin();
    }

    public function getAllUserSelectBox($emptyOption = true)
    {
        $all = $this->repository->pluckAll();
        if ($emptyOption) {
            array_unshift($all, 'Tất cả');
        }
        return $all;
    }

    public function updateProfile(UserRequest $request)
    {
        $userData = $request->except('_method', '_token', 'password_confirmation', 'image');
        if ($request->hasFile('image')) {
            $path = $request->file('image')->store(USER_IMAGE_PATH, 'public');
            $userData['avatar'] = $path;
        }
        $userData = array_filter($userData);
        if ($this->findAndUpdate(auth()->id(), $userData)) {
            if ($request->ajax()) {
                return [
                    'message' => 'Cập nhật thành công',
                    'code' => 200,
                    'data' => [
                        'url' => route('users.profile')
                    ]
                ];
            }
            return redirect()->back()->with('success', 'Cập nhật thông tin thành công');
        }
        if ($request->ajax()) {
            return [
                'message' => 'Cập nhật thất bại',
                'code' => 500,
                'data' => [
                    'url' => route('users.profile')
                ]
            ];
        }
        return redirect()->back()->with('error', 'Cập nhật thông tin thất bại');
    }
}
