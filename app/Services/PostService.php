<?php

namespace App\Services;

use App\Repositories\Post\IPostRepository;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Carbon\Carbon;
use App\Http\Requests\Admin\PostRequest;

class PostService extends BaseService 
{


    public function __construct(IPostRepository $postRepository)
    {
        $this->repository = $postRepository;
    }

    public function getAsJson($filters = [])
    {
        return $this->repository->getAsJson($filters, ['user', 'category']);
    }

    public function savePost(PostRequest $request)
    {
        $path = $request->file('image')->store(POST_IMAGE_PATH, 'public');
        $postData = $request->only('category_id', 'title', 'description', 'content', 'source', 'published_at', 'expired_at');
        if (!empty($postData['published_at'])) {

            $postData['published_at'] = Carbon::parse($postData['published_at'])->format('Y-m-d H:i:s');
        }
        if (!empty($postData['expired_at'])) {

            $postData['expired_at'] = Carbon::parse($postData['expired_at'])->format('Y-m-d H:i:s');
        }
        $postData['image'] = $path;
        $postData['slug'] = Str::slug($postData['title']);
        $postData['user_id'] = auth()->id();
        if ($this->insert($postData)) {
            return [
                'message' => 'Thêm mới thành công',
                'code' => 200,
                'data' => [
                    'url' => route('posts.index')
                ]
            ];
        }

        return [
            'message' => 'Thêm mới thất bại',
            'code' => 500,
            'data' => [
                'url' => route('posts.index')
            ]
        ];
    }
    
    public function findAndUpdatePost($id, PostRequest $request)
    {
        $postData = $request->only('category_id', 'title', 'content', 'description', 'source', 'published_at', 'expired_at');
        if (!empty($postData['published_at'])) {

            $postData['published_at'] = Carbon::parse($postData['published_at'])->format('Y-m-d H:i:s');
        }
        if (!empty($postData['expired_at'])) {

            $postData['expired_at'] = Carbon::parse($postData['expired_at'])->format('Y-m-d H:i:s');
        }

        if ($request->hasFile('image')) {
            $path = $request->file('image')->store(POST_IMAGE_PATH, 'public');
            $postData['image'] = $path;
        }
        $postData['slug'] = Str::slug($postData['title']);
        if (parent::findAndUpdate($id, $postData)) {
            if ($request->ajax()) {
                return [
                    'message' => 'Cập nhật thành công',
                    'code' => 200,
                    'data' => [
                        'url' => route('posts.edit', $id)
                    ]
                ];
            }
            return redirect()->back()->with('success', 'Cập nhật thông tin thành công');
        }
        if ($request->ajax()) {
            return [
                'message' => 'Cập nhật thất bại',
                'code' => 500,
                'data' => [
                    'url' => route('posts.edit', $id)
                ]
            ];
        }
        return redirect()->back()->with('error', 'Cập nhật thông tin thất bại');
    }

    public function findByCategoryId($categoryId, $limit = POST_BY_CATEGORY_LIMIT)
    {
        return $this->repository->findByCategoryId($categoryId, $limit);
    }

    public function randomByCategoryId($categoryId, $limit = POST_BY_CATEGORY_LIMIT)
    {
        return $this->repository->randomByCategoryId($categoryId, $limit);
    }
    
    public function getFirstByCategoryAndType($categoryId, $type = 1)
    {
        return $this->repository->getFirstByCategoryAndType($categoryId, $type);
    }

    public function findPostBySlug($slug, $relations = [])
    {
        return $this->repository->findPostBySlug($slug, $relations = []);
    }

    public function searchPost($search)
    {
        return $this->repository->searchPost($search, PAGINATE);
    }

    public function paginatePostByCategoryId($categoryId)
    {
        return $this->repository->paginatePostByCategoryId($categoryId, PAGINATE);
    }
}