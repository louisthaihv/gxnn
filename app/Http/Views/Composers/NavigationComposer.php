<?php

namespace App\Http\Views\Composers;

use App\Services\CategoryService;
use Illuminate\View\View;

class NavigationComposer
{
    private $navigation;
    private $thongTin;
    private $thanhLe;

    public function __construct(CategoryService $categoryService)
     {
        $this->navigation = $categoryService->getMenu();
        $this->thongTin = $categoryService->find(THONG_TIN_ID, ['posts']);
        $this->thanhLe = $categoryService->find(THANH_LE_ID, ['posts']);
     }
    /**
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('navigation', $this->navigation);
        $view->with('thongTin', $this->thongTin);
        $view->with('thanhLe', $this->thanhLe);
    }
}