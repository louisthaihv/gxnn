<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Services\HomeService;

class HomeController extends Controller
{
    protected $homeService;

    public function __construct(HomeService $homeService) {
        $this->homeService = $homeService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $viewData = $this->homeService->getHomeData();
        return view('frontend.index')->with(compact('viewData'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($slug)
    {
        $viewData = $this->homeService->getSinglePostData($slug);
        return view('frontend.single')->with('viewData', $viewData);
    }

    public function search(Request $request)
    {
        $posts = $this->homeService->searchPost($request->get('search'));
        return view('frontend.post-search')->with('posts', $posts);
    }

    public function categoryPost($categoryId)
    {
        $viewData = $this->homeService->getCategoryAndPagingPost($categoryId);
        return view('frontend.category-posts')->with('viewData', $viewData);
    }

    public function sendMail(Request $request)
    {
        return $this->homeService->sendMail($request->all());
    }
}
