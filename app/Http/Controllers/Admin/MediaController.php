<?php

namespace App\Http\Controllers\Admin;

use App\Services\UploadFileService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class MediaController extends Controller {
    public function __construct() {

    }

    public function upload(Request $request) {
        try {

            $fileName = (new UploadFileService($request->file('file'), 'medias'))->upload();

            return response()->json(['location' => '/' . $fileName['content_path']]);
        } catch (\Exception $exception) {
            Log::error('Something went wrong when insert banner ' . $exception->getMessage());

            return response()->json(['error' => 'Something went wrong'], 500);
        }


    }
}
