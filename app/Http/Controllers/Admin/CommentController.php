<?php

namespace App\Http\Controllers\Admin;

use App\Services\CommentService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class CommentController extends Controller
{
    protected $commentService;
    public function __construct(CommentService $commentService) {
        $this->commentService = $commentService;
    }

    public function index() {
        return view('admin.comments.index');
    }

    public function asJson(Request $request) {
        return $this->commentService->getAsJson($request->only('is_reviewed'));
    }

    public function updateIsReviewed(Request $request, $id) {
        try{
            $this->commentService->findAndUpdate($id, $request->only('is_reviewed'));
            return response()->json([
                'code' => 200,
                'message' => 'update successfully'
            ]);
        }catch (\Exception $exception){
            Log::error('Something went wrong when update comment status ' . $exception->getMessage());

            return response()->json([
                'code' => 500,
                'message' => 'Internal server'
            ]);
        }

    }
}
