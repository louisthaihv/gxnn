<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\DocumentIsReviewedRequest;
use App\Http\Requests\DocumentRequest;
use App\Services\DocumentService;
use App\Services\LevelService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Log\Logger;
use Illuminate\Support\Facades\Log;
use Illuminate\Support\Str;

class DocumentController extends Controller {
    protected $documentService;
    protected $levelService;

    public function __construct(DocumentService $documentService, LevelService $levelService) {
        $this->documentService = $documentService;
        $this->levelService = $levelService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        return view('admin.documents.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $levels = $this->levelService->pluckAll();

        return view('admin.documents.create', compact('levels'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  DocumentRequest $request
     * @return \Illuminate\Http\Response
     */
    public function store(DocumentRequest $request) {
        try {
            $request->merge(['slug' => Str::slug($request->get('name')), 'user_id' => auth()->user()->id]);

            $document = $this->documentService->createAndUploadDocument($request);

            return response()->json(['code' => 200, 'message' => 'Create document successfully', 'data' => $document], 200);
        } catch (\Exception $exception) {
            return response()->json(['code' => 500, 'message' => 'Internal server error . ' . $exception->getMessage()], 500);
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $document = $this->documentService->find($id);

        $levels = $this->levelService->pluckAll();

        return view('admin.documents.edit', compact('document', 'levels'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id) {
        try {
            $this->documentService->findAndUpdateDocument($id, $request);

            return response()->json(['code' => 200, 'message' => 'Update successfully']);
        } catch (\Exception $exception) {
            Log::error('Something went wrong when delete document page ' . $exception->getMessage());
            return response()->json(['code' => 500, 'message' => 'Delete successfully'], 500);
        }

    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            $this->documentService->findAndDelete($id);

            return response()->json(['code' => 200, 'message' => 'Delete document successfully']);
        } catch (\Exception $exception) {
            Log::error('Something went wrong when delete ' . __METHOD__);
            return response()->json(['code' => 500, 'message' => 'Something went wrong when delete document']);
        }

    }

    public function asJson(Request $request) {
        return $this->documentService->getAsJson($request->only('is_reviewed'));
    }

    public function updateIsReviewed(DocumentIsReviewedRequest $request, $id) {
        $document = $this->documentService->findAndUpdate($id, $request->only('is_reviewed'));

        if (!$document) {
            return response()->json(['code' => 500, 'Đã xảy ra lỗi trong quá trình thao tác']);
        }
        return response()->json(['code' => 200, 'Cập nhật thành công']);
    }

    public function deleteDocumentPage(Request $request) {
        try {
            $this->documentService->deleteDocumentPage($request->get('documentId'), $request->get('id'));

            return response()->json(['code' => 200, 'message' => 'Delete successfully']);
        } catch (\Exception $exception) {
            Log::error('Something went wrong when delete document page ' . $exception->getMessage());
            return response()->json(['code' => 500, 'message' => 'Delete successfully'], 500);
        }

    }
}
