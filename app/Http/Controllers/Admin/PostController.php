<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Services\CategoryService;
use Illuminate\Http\Request;
use App\Http\Requests\Admin\PostRequest;

use App\Services\PostService;
use App\Services\UserService;

class PostController extends Controller
{
    public function __construct(PostService $postService, UserService $userService, CategoryService $categoriService)
    {
        $this->service = $postService;
        $this->userService = $userService;
        $this->categoriService = $categoriService;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $allUser = $this->userService->getAllUserSelectBox();
        return view('admin.posts.index')->with(compact('allUser'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $categoriesSelect = $this->categoriService->getAllCategorySelectBox();
        return view('admin.posts.create')->with(compact('categoriesSelect'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(PostRequest $request)
    {
        return $this->service->savePost($request);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = $this->service->find($id);
        $categoriesSelect = $this->categoriService->getAllCategorySelectBox();
        return view('admin.posts.edit')->with(compact('post', 'categoriesSelect'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(PostRequest $request, $id)
    {
        return $this->service->findAndUpdatePost($id, $request);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $this->service->findAndDelete($id);

        return response()->json(['code' => 200, 'msg' => 'Đã xoá']);
    }
    /**
     * Undocumented function
     *
     * @param Request $request
     * @return json
     */
    public function asJson(Request $request)
    {
        return $this->service->getAsJson($request->only('user_id', 'type'));
    }
}
