<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\Admin\BannerRequest;
use App\Services\BannerService;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Log;

class BannerController extends Controller {
    protected $bannerService;

    public function __construct(BannerService $bannerService) {
        $this->bannerService = $bannerService;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $banners = $this->bannerService->getCurrentBanners();

        return view('admin.banners.index', compact('banners'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('admin.banners.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(BannerRequest $request) {
        try {
            $this->bannerService->uploadAndStore($request);
            return redirect()->route('banners.index')->with('success', 'Thêm mới banner thành công');
        } catch (\Exception $exception) {
            Log::error('Something went wrong when insert banner ' . $exception->getMessage());
            return redirect()->back()->with('error', 'Xảy ra lỗi trong thao tác');
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $banner = $this->bannerService->find($id);

        return view('admin.banners.edit', compact('banner'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(BannerRequest $request, $id) {
        try {
            $this->bannerService->uploadAndUpdate($request, $id);
            return redirect()->route('banners.index')->with('success', 'Cập nhật banner thành công');
        } catch (\Exception $exception) {
            Log::error('Something went wrong when insert banner ' . $exception->getMessage());
            return redirect()->back()->with('error', 'Xảy ra lỗi trong thao tác');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        try {
            $this->bannerService->findAndDelete($id);
            return response()->json(['code' => 200, 'message' => 'Delete banner successfully']);
        } catch (\Exception $exception) {
            Log::error('Something went wrong when delete banner ' . $exception->getMessage());
            return response()->json(['code' => 500, 'message' => 'Delete banner successfully'], 500);
        }

    }

    public function asJson(Request $request) {
        return $this->bannerService->getAsJson($request->only('user_id'));
    }
}
