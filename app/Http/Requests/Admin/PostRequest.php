<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\JsonResponse;

class PostRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [];
        if ($this->isMethod('post')) {
            $rules = [
                'category_id' => 'required',
                'title' => 'required|unique:posts|max:255',
                'content' => 'required',
                'image' => 'required'
            ];
        }
        if ($this->isMethod('put')) {
            $rules = [
                'category_id' => 'required',
                'title' => 'required|max:255|unique:posts,' . $this->id,
                'content' => 'required',
                'image' => 'required'
            ];
        }
        return $rules;
    }

    public function messages()
    {
        return [
            'category_id.required' => 'Chọn đề mục cho bài viết',
            'title.required' => 'Phải nhập tiêu đề bài viết',
            'content.required' => 'Nội dung bài viết không được trống',
            'image.required' => 'Ảnh không được trống',
            'title.unique' => 'Tiêu đề đã tồn tại',
        ];
    }
    public function failedValidation(Validator $validator) {
        $error = $validator->messages()->toJson();
        return new JsonResponse($error);
   }
}
