<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', ['as' => 'index', 'uses' => 'HomeController@index']);
Route::get('/search', ['as' => 'post.search', 'uses' => 'HomeController@search']);
Route::get('/post/{slug}', ['as' => 'post.slug', 'uses' => 'HomeController@show']);
Route::get('/category/{id}/posts', ['as' => 'category.posts', 'uses' => 'HomeController@categoryPost']);
Route::post('/send-email', ['as' => 'email.send', 'uses' => 'HomeController@sendMail']);

Auth::routes();
Route::group(['namespace' => 'Admin'], function () {
    Route::post('media/upload', 'MediaController@upload');
});
Route::group(
    [
        'prefix' => 'admin',
        'middleware' => ['auth', 'isManager'] ,
        'namespace' => 'Admin'
    ], function () {
        Route::get(
            '/',
            [
                'as' => 'admin.dashboard',
                'uses' => 'DashboardController@index'
            ]
        );

        Route::get(
            'users/as-json',
            [
                'uses' => 'UserController@asJson',
                'as' => 'admin.users.as.json'
            ]
        );
        Route::get(
            'users/profile-setting',
            [
                'uses' => 'UserController@getProfile',
                'as' => 'users.profile'
            ]
        );
        Route::put(
            'users/profile-setting',
            [
                'uses' => 'UserController@updateProfile',
                'as' => 'users.profile'
            ]
        );
        Route::get(
            'posts/as-json', 
            [
                'uses' => 'PostController@asJson',
                'as' => 'admin.posts.as.json'
            ]
        );
        Route::get(
            'categories/as-json', 
            [
                'uses' => 'CategoryController@asJson',
                'as' => 'admin.categories.as.json'
            ]
        );
        Route::resources(
            [
                'users' => 'UserController',
                'categories' => 'CategoryController',
                'posts' => 'PostController',
            ]
        );
    }
);
