var categoryTable;
$(function () {
    var categoryTableId = $('#categoryTable')
    categoryTable = categoryTableId.DataTable({
        ajax: {
            url: categoryUrlJsonData
        },
        processing: true,
        serverSide: true,
        columns: [
            {data: 'id', title: 'ID', name: 'id'},
            {data: 'title', title: 'Title', name: 'title'},
            {data: 'type', title: 'Type', name: 'type'},
            {
                data: 'id', title: 'Thao tác ', name: 'id', sortable: false, searchable: false,
                render: function (data, type, row, meta) {
                    return '<div class="icon-button-demo">' +
                        '<a href=' + categoryUrlPage + '/' + data + '/edit' + ' type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float"><i class="material-icons">edit</i></a>' +
                        '</div>';
                }
            },

        ]
    });
});

function deleteCategory(categoryId) {
    return deleteItem(categoryUrlPage + '/' + categoryId, categoryTable);
}

