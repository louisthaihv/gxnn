var userTable;
$(function () {
    var userTableId = $('#userTable'), roleId = $('#roleId'), isActive = $('#isActive'), canUpload = $('#canUpload');
    userTable = userTableId.DataTable({
        ajax: {
            url: userUrlJsonData,
            data: function (data) {
                data.role_id = roleId.val();
                data.is_active = isActive.val();
                data.can_upload = canUpload.val();
            }
        },
        processing: true,
        serverSide: true,
        columns: [
            {data: 'id', title: 'ID', name: 'id'},
            {data: 'name', title: 'Họ tên', name: 'name'},
            {data: 'email', title: 'Email', name: 'email'},
            {data: 'phone_number', title: 'Số điện thoại', name: 'phone_number'},
            {
                data: 'active_name',
                title: 'Trạng thái',
                name: 'active_name',
                sortable: false,
                searchable: false,
                render: function (data, type, row, meta) {
                    if (parseInt(row.is_active) === 1) {
                        return '<button class="btn bg-teal btn-block btn-xs waves-effect">' + data + '</button>';
                    }
                    return '<button class="btn bg-red btn-block btn-xs waves-effect">' + data + '</button>';

                }
            },

            {data: 'is_active', title: 'Trạng thái', name: 'is_active', visible: false},
            {data: 'role_name', title: 'Nhóm quản trị', name: 'role_name', sortable: false, searchable: false},

            {
                data: 'id', title: 'Thao tác ', name: 'id', sortable: false, searchable: false,
                render: function (data, type, row, meta) {
                    return '<div class="icon-button-demo">' +
                        '<a href=' + userUrlPage + '/' + data + '/edit' + ' type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float"><i class="material-icons">edit</i></a>' +
                        '<button id="deleteUser" onclick="javascript: deleteUser(' + data + ')" type="button" class="btn btn-danger btn-circle waves-effect waves-circle waves-float"><i class="material-icons">delete</i></button></div>';
                }
            },

        ]
    });
    roleId.add(isActive).add(canUpload).on('change', function (e) {
        e.preventDefault();
        userTable.ajax.reload();
    });


});

function deleteUser(userId) {
    return deleteItem(userUrlPage + '/' + userId, userTable);
}

