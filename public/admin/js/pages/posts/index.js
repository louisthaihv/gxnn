var postTable;
$(function () {
    var postTableId = $('#postTable'), userId = $('#userId'), postType = $('#postType'), active = $('#active');
    postTable = postTableId.DataTable({
        ajax: {
            url: postUrlJsonData,
            data: function (data) {
                data.user_id = userId.val();
                data.type = postType.val();
                data.active = active.val();
            }
        },
        processing: true,
        serverSide: true,
        columns: [
            {data: 'id', title: 'ID', name: 'id'},
            // {data: 'user_id', title: 'Người Viết', name: 'user_id', render: function (data, type, row, meta){
            //     return `<a href='${userUrlPage}/${data}' target='_blank'>${row.user.name}</a>`;
            // }},
            {data: 'category_id', title: 'Mục', name: 'category_id', render: function (data, type, row, meta){
                return `<a href='${categoryUrlPage}/${data}' target='_blank'>${row.category.title}</a>`;
            }},
            {data: 'title', title: 'Tiêu Đề', name: 'title'},
            
            {data: 'type', title: 'Loại', name: 'type', render: function(data, type, row, meta){
                if (data == 1) {
                    return 'bài viết';
                }
                return 'video';
            }},
            {data: 'active', title: 'Trạng thái', name: 'active', render: function(data, type, row, meta){
                if (data == 1) {
                    return 'Đang hiển thị';
                }
                return 'Đã gỡ';
            }},
            {data: 'description', title: 'Mô tả ngắn', name: 'description'},
            // {data: 'published_at', title: 'Ngày Xuất Bản', name: 'published_at'},
            // {data: 'expired_at', title: 'Ngày Hết Hạn', name: 'expired_at'},
            {
                data: 'id', title: 'Thao tác ', name: 'id', sortable: false, searchable: false,
                render: function (data, type, row, meta) {
                    return '<div class="icon-button-demo">' +
                        '<a href=' + postUrlPage + '/' + data + '/edit' + ' type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float"><i class="material-icons">edit</i></a>' +
                        '<button id="deleteUser" onclick="javascript: deleteUser(' + data + ')" type="button" class="btn btn-danger btn-circle waves-effect waves-circle waves-float"><i class="material-icons">delete</i></button></div>';
                }
            },

        ]
    });
    userId.on('change', function (e) {
        e.preventDefault();
        postTable.ajax.reload();
    });
    postType.on('change', function (e) {
        e.preventDefault();
        postTable.ajax.reload();
    });
    active.on('change', function (e) {
        e.preventDefault();
        postTable.ajax.reload();
    });
});

function deleteUser(postId) {
    return deleteItem(postUrlPage + '/' + postId, postTable);
}

