﻿var error = function (error = "Lỗi!", msg = "Đã xảy ra lỗi trong thao tác") {
    swal(error, msg, "error");
};

var success = function (msg = "Xoá thành công.") {
    swal("Xử lý thành công!", msg, "success");
};

var confirmAction = function (url, dataTable, callback) {
    swal({
        title: "Cẩn thận khi thực hiện thao tác này ?",
        text: "Hãy kiểm tra kỹ trước khi thực hiện !",
        type: "warning",
        showCancelButton: true,
        confirmButtonColor: "#DD6B55",
        confirmButtonText: "Chắc chắn thực hiện thao tác này  !",
        cancelButtonText: "Bỏ qua!",
        closeOnConfirm: false,
        closeOnCancel: false
    }, function (isConfirm) {
        if (isConfirm) {
            callback();
        } else {
            error('Bạn đã bỏ qua thao tác này');
            dataTable.ajax.reload();
        }

    });
};


var deleteItem = function (url, dataTable) {
    confirmAction(url, dataTable, function () {
        $.ajax({
            method: 'DELETE',
            url: url
        }).success(function (response) {
            if (response.code === 200) {
                success();
            } else {
                error();
            }
            dataTable.ajax.reload();
        }).error(function () {
            error();
            dataTable.ajax.reload();
        })

    });
};

var showAlert = function (title, content) {
    swal({
        title: title,
        text: content,
        html: true
    });
};


var toggleAction = function (url, data, dataTable) {
    confirmAction(url, dataTable, function () {
        $.ajax({
            method: 'PUT',
            url: url,
            data
        }).success(function (response) {
            if (response.code === 200) {
                success('Cập nhật thành công');
            } else {
                error('Bỏ qua');
            }
            dataTable.ajax.reload();
        }).error(function () {
            error();
            dataTable.ajax.reload();
        });

    })
};

function addCommas(num) {
    return num;
    return num.toString().replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1,")
}
