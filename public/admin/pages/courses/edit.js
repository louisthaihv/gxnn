$(function () {

    var form = $('#wizard_with_validation').show();
    var mediaFiles = [];
    form.steps({
        headerTag: 'h3',
        bodyTag: 'fieldset',
        transitionEffect: 'slideLeft',
        onInit: function (event, currentIndex) {
            $.AdminBSB.input.activate();
            $('select').selectpicker('refresh');
            var $tab = $(event.currentTarget).find('ul[role="tablist"] li');
            var tabCount = $tab.length;
            $tab.css('width', (100 / tabCount) + '%');

            setButtonWavesEffect(event);

            getSubjects(thematicId);
            $("select#thematicId").on("changed.bs.select", function (e, clickedIndex, newValue, oldValue) {
                var thematicId = $(this).val();
                getSubjects(thematicId, false);
            });


        },
        onStepChanging: function (event, currentIndex, newIndex) {
            if (currentIndex > newIndex) {
                return true;
            }

            if (currentIndex < newIndex) {
                form.find('.body:eq(' + newIndex + ') label.error').remove();
                form.find('.body:eq(' + newIndex + ') .error').removeClass('error');
            }

            form.validate().settings.ignore = ':disabled,:hidden';
            return form.valid();
        },
        onStepChanged: function (event, currentIndex, priorIndex) {
            setButtonWavesEffect(event);
        },
        onFinishing: function (event, currentIndex) {
            form.validate().settings.ignore = ':disabled';
            return form.valid();
        },
        onFinished: function (event, currentIndex) {
            var data = $('form#wizard_with_validation').serializeArray();
            var formData = new FormData();
            $.each(data, function (key, el) {
                formData.append(el.name, el.value);
            });
            formData.set('thumbnail', $('#favicon-upload')[0].files[0]);

            for (var i = 0; i < mediaFiles.length; i++) {
                formData.set('class.media[' + i + ']', mediaFiles[i]);
            }

            $.ajax({
                method: 'POST',
                url: urlCoursesPage + '/' + $('#courseId').val() + '/update',
                data: formData,
                processData: false,
                contentType: false,
            }).success(function (response) {
                if (response.code === 200) {
                    success('Tạo mới thành công');
                    window.location.href = urlCoursesPage;
                } else {
                    error();
                }

            }).fail(function (err) {
                error();
                var errorsMsg = '<ul class="list-group errors">';

                for (var i in err.errors) {
                    errorsMsg += '<li style="border-bottom: 2px solid #fff" class="list-group-item list-group-item-danger">';
                    errorsMsg += err.errors[i].shift();
                    errorsMsg += '</li>';
                }

                errorsMsg += '</ul>';

                $('.body').find('.errors').remove();
                $('.body').prepend(errorsMsg);
            });

        }
    });

    form.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        },
    });

    $('#lessonFile').on('change', function (e) {
        e.preventDefault();
        mediaFiles.push(e.target.files[0]);
        $('#fileName').html($(this).val());
    });

    $('#createLesson').on('click', function (e) {
        e.preventDefault();
        $('#createLessonForm').validate({
            highlight: function (input) {
                $(input).parents('.form-line').addClass('error');
            },
            unhighlight: function (input) {
                $(input).parents('.form-line').removeClass('error');
            },
            errorPlacement: function (error, element) {
                $(element).parents('.form-group').append(error);
            },
        });
        if ($('#createLessonForm').valid()) {
            $('#defaultModal').modal('hide');
            var lessonName = $('#lessonName').val(), lessonDescription = $('#lessonDescription').val(),
                lessonFile = $('#lessonFile').val();
            var lessonsList = $('#lessonsList');
            var tbody = lessonsList.find('table').find('tbody');
            var stt = tbody.find('tr').length;
            tbody.append(`<tr>
                            <th scope="row">${stt + 1}</th>
                            <td>${lessonName}<input value="${lessonName}" class="lesson-name" type="hidden" name="class.name[]"></td></td>
                            <td>${lessonDescription}<input type="hidden" class="lesson-description" value="${lessonDescription}" name="class.description[]"></td>
                            <td>${lessonFile}<input type="file" class="media-files lesson-file" style="display: none;" id="lessonMedia${stt}" value="${lessonFile}" name="class.media[]"></td>
                        </tr>`);
        }

    });
});

function setButtonWavesEffect(event) {
    $(event.currentTarget).find('[role="menu"] li a').removeClass('waves-effect');
    $(event.currentTarget).find('[role="menu"] li:not(.disabled) a').addClass('waves-effect');
}


function getSubjects(thematicId, hasDefaultValue = true) {
    $.ajax({
        method: 'GET',
        url: urlGetSubjectsByCourse,
        data: {thematic_id: thematicId}
    }).success(function (response) {
        var html = '';
        $.each(response.subjects, function (index, subject) {
            if (hasDefaultValue && subject.id == subjectId) {
                html += `<option selected="true" value="${subject.id}">${subject.name}</option>`;
            } else {
                html += `<option value="${subject.id}">${subject.name}</option>`;
            }
        });
        $('#subjectId').html(html);
        $('#subjectId').selectpicker('refresh');
    });
}

