var courseTable;
$(function () {
    var courseTableId = $('#courseTable'), isReviewed = $('#isReviewed');
    courseTable = courseTableId.DataTable({
        ajax: {
            url: courseUrlJsonData,
            data: function (data) {
                data.is_reviewed = isReviewed.val();
            }
        },
        processing: true,
        serverSide: true,
        order: [[0, "desc"]],
        columns: [
            {data: 'id', title: 'ID', name: 'id'},
            {data: 'name', title: 'Tên khoá học', name: 'name'},
            {data: 'thumbnail_path', title: 'Tên khoá học', name: 'thumbnail_path', visible: false},
            {data: 'thumbnail_path', title: 'Thumbnail', name: 'thumbnail_path', searchable: false, orderable: false, render: function(data) {
                    return `<img class="img-responsive thumbnail" src="/${data}"/>`;
                }
            },
            {data: 'user.name', title: 'Người tạo', name: 'user.name'},
            {data: 'created_at', title: 'Ngày tạo', name: 'created_at'},
            {data: 'price', title: 'Giá', name: 'price', visible: false},
            {data: 'thematic.name', title: 'Chuyên đề', name: 'thematic.name'},
            {data: 'subject.name', title: 'Môn học', name: 'subject.name'},
            {data: 'pretty_price', title: 'Giá', name: 'pretty_price', sortable: false, searchable: false},
            {
                data: 'id', title: 'Thao tác ', name: 'id', sortable: false, searchable: false,
                render: function (data) {
                    return '<div class="icon-button-demo">' +
                        '<a href=' + courseUrlPage + '/' + data + '/edit' + ' type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float"><i class="material-icons">edit</i></a>' +
                        '<button id="deleteUser" onclick="javascript: deleteCourse(' + data + ')" type="button" class="btn btn-danger btn-circle waves-effect waves-circle waves-float"><i class="material-icons">delete</i></button></div>';
                }
            },
        ]
    });
    // isReviewed.on('change', function (e) {
    //     e.preventDefault();
    //     courseTable.ajax.reload();
    // });
});

function deleteCourse(courseId) {
    return deleteItem(courseUrlPage + '/' + courseId, courseTable);
}

