var editUser = $('#createDocument');

$(function() {
    editUser.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });

    Dropzone.options.createDocument = {
        paramName: "document_files",
        maxFilesize: 10,
        maxFiles: 5,
        uploadMultiple: true,
        addRemoveLinks: true,
        acceptedFiles: '.jpg, .jpeg, .png, .pdf',
        dictRemoveFile: 'Xoá file',
        autoProcessQueue: false,
        removedfile: function(file) {
            swal({
                title: "Cẩn thận khi thực hiện thao tác này ?",
                text: "Hãy kiểm tra kỹ trước khi thực hiện !",
                type: "warning",
                showCancelButton: true,
                confirmButtonColor: "#DD6B55",
                confirmButtonText: "Chắc chắn thực hiện thao tác này  !",
                cancelButtonText: "Bỏ qua!",
                closeOnConfirm: false,
                closeOnCancel: false
            }, function (isConfirm) {
                if (isConfirm) {
                    $.ajax({
                        method: 'POST',
                        url: urlDeleteDocumentPage,
                        data: {
                            id: file.id,
                            documentId: file.documentId
                        }
                    }).done(function(response) {
                        file.previewElement.remove();
                        success('Bạn đã xoá thành công');
                    }).fail(function(err) {
                        error();
                    });
                } else {
                    error('Bạn đã bỏ qua thao tác');
                }
            });
        },
        init: function () {
            var submitButton = document.querySelector("#createBtn");
            var myDropzone = this;

            images.forEach(function(image) {
                myDropzone.options.addedfile.call(myDropzone, image);
                myDropzone.options.thumbnail.call(myDropzone, image, image.name);

            });

            submitButton.addEventListener("click", function (e) {
                e.preventDefault();
                e.stopPropagation();
                if(editUser.valid()){
                    myDropzone.processQueue();
                }
            });
            this.on('sending', function (file, xhr, formData) {
                var data = $('form').serializeArray();
                $.each(data, function (key, el) {
                    formData.append(el.name, el.value);
                });
            });



            this.on('removedfile', function(file) {
                // swal({
                //     title: "Cẩn thận khi thực hiện thao tác này ?",
                //     text: "Hãy kiểm tra kỹ trước khi thực hiện !",
                //     type: "warning",
                //     showCancelButton: true,
                //     confirmButtonColor: "#DD6B55",
                //     confirmButtonText: "Chắc chắn thực hiện thao tác này  !",
                //     cancelButtonText: "Bỏ qua!",
                //     closeOnConfirm: false,
                //     closeOnCancel: false
                // }, function (isConfirm) {
                //     if (isConfirm) {
                //         $.ajax({
                //             method: 'POST',
                //             url: urlDeleteDocumentPage,
                //             data: {
                //                 id: file.id,
                //                 documentId: file.documentId
                //             }
                //         }).done(function(response) {
                //             success('Bạn đã xoá thành công');
                //         }).fail(function(err) {
                //             error();
                //         });
                //     } else {
                //         error('Bạn đã bỏ qua thao tác');
                //         var _ref;
                //         return (_ref = file.previewElement) != null ? _ref.parentNode.removeChild(file.previewElement) : void 0;
                //     }
                // });

            });

            this.on('success', function () {
                // window.location.href = urlDocumentPage;
            });

            this.on('error', function (file, response) {
                var errorsMsg = '<ul class="list-group error-msg">';

                if(response.code === 500) {
                    errorsMsg += '<li style="border-bottom: 2px solid #fff" class="list-group-item list-group-item-danger">';
                    errorsMsg += 'Đã xảy ra lỗi trong thao tác, vui lòng thử lại';
                    errorsMsg += '</li>';
                } else{
                    for (var i in response.errors) {
                        errorsMsg += '<li style="border-bottom: 2px solid #fff" class="list-group-item list-group-item-danger">';
                        errorsMsg += response.errors[i].shift();
                        errorsMsg += '</li>';
                    }
                }
                errorsMsg += '</ul>';

                $('.body').find('.error-msg').remove();
                $('.body').prepend(errorsMsg);

            });
            this.on('maxfilesexceeded', function(file) {
                myDropzone.removeFile( file );
            })
        }
    };

});
