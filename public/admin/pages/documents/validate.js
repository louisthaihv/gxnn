var editUser = $('#createDocument');

$(function() {
    editUser.validate({
        highlight: function (input) {
            $(input).parents('.form-line').addClass('error');
        },
        unhighlight: function (input) {
            $(input).parents('.form-line').removeClass('error');
        },
        errorPlacement: function (error, element) {
            $(element).parents('.form-group').append(error);
        }
    });


    Dropzone.options.createDocument = {
        paramName: "document_files",
        maxFilesize: 10,
        maxFiles: 5,
        uploadMultiple: true,
        addRemoveLinks: true,
        acceptedFiles: '.jpg, .jpeg, .png, .pdf',
        dictRemoveFile: 'Xoá file',
        autoProcessQueue: false,
        init: function () {
            var submitButton = document.querySelector("#createBtn");
            var myDropzone = this;
            submitButton.addEventListener("click", function (e) {
                e.preventDefault();
                e.stopPropagation();
                if(editUser.valid()){
                    myDropzone.processQueue();
                }
            });
            this.on('sending', function (file, xhr, formData) {
                var data = $('form').serializeArray();
                $.each(data, function (key, el) {
                    formData.append(el.name, el.value);
                });
            });
            this.on('success', function () {
                window.location.href = urlDocumentPage;
            });

            this.on('error', function (file, response) {
                var errorsMsg = '<ul class="list-group error-msg">';

                if(response.code === 500) {
                    errorsMsg += '<li style="border-bottom: 2px solid #fff" class="list-group-item list-group-item-danger">';
                    errorsMsg += 'Đã xảy ra lỗi trong thao tác, vui lòng thử lại';
                    errorsMsg += '</li>';
                } else{
                    for (var i in response.errors) {
                        errorsMsg += '<li style="border-bottom: 2px solid #fff" class="list-group-item list-group-item-danger">';
                        errorsMsg += response.errors[i].shift();
                        errorsMsg += '</li>';
                    }
                }
                errorsMsg += '</ul>';

                $('.body').find('.error-msg').remove();
                $('.body').prepend(errorsMsg);

            });
            this.on('maxfilesexceeded', function(file) {
                myDropzone.removeFile( file );
            })
        }
    };

});
