var documentTable;
$(function () {
    var documentTableId = $('#documentTable'), isReviewed = $('#isReviewed');
    documentTable = documentTableId.DataTable({
        ajax: {
            url: documentUrlJsonData,
            data: function (data) {
                data.is_reviewed = isReviewed.val();
            }
        },
        processing: true,
        serverSide: true,
        order: [[0, "desc"]],
        columns: [
            {data: 'id', title: 'ID', name: 'id'},
            {data: 'slug', title: 'slug', name: 'slug', visible: false},
            {data: 'name', title: 'Tên tài liệu', name: 'name', render: function(data, type, row, meta) {
                    return `<a href="${documentUrlPage + '/' + row.slug}" target="_blank" style="text-align: left" class="btn bg-amber btn-block btn-xs waves-effect"><i class="material-icons">remove_red_eye</i> <span class="icon-name">${data}</span> </a>`;
                }
            },
            {data: 'level.name', title: 'Trình độ', name: 'level.name'},
            {data: 'user.name', title: 'Người tạo', name: 'user.name'},
            {data: 'downloads', title: 'Lượt tải', name: 'downloads'},
            {data: 'views', title: 'Lượt xem', name: 'views'},
            {data: 'is_reviewed', title: 'is_reviewed', name: 'is_reviewed', visible: false},
            {data: 'created_at', title: 'Ngày tạo', name: 'created_at'},
            {
                data: 'is_reviewed_name',
                title: 'Trạng thái',
                name: 'is_reviewed_name',
                sortable: false,
                searchable: false,
                render: function (data, type, row, meta) {
                    var status = parseInt(row.is_reviewed);
                    var checked = status === 1 ? 'checked' : '';

                    return "<div class=\"demo-switch\">" +
                        "                                <div class=\"switch\">" +
                        "                                    <label>Chưa duyệt<input onchange='javascript: updateIsReviewed(" + row.id + ',' + row.is_reviewed + ")' class=\"chk-col-deep-purple\" type=\"checkbox\" " + checked + "><span class=\"lever\"></span>Đã duyệt</label>" +
                        "                                </div>" +
                        "                            </div>";

                }
            }, {
                data: 'id', title: 'Thao tác ', name: 'id', sortable: false, searchable: false,
                render: function (data, type, row, meta) {
                    return '<div style="display: inline" class="icon-button-demo">' +
                        '<a href=' + documentUrlPage + '/' + data + '/edit' + ' type="button" class="btn btn-success btn-circle waves-effect waves-circle waves-float"><i class="material-icons">edit</i></a>' +
                        '<button id="deleteUser" onclick="javascript: deleteDocument(' + data + ')" type="button" class="btn btn-danger btn-circle waves-effect waves-circle waves-float"><i class="material-icons">delete</i></button></div>';
                }
            },

        ]
    });
    isReviewed.on('change', function (e) {
        e.preventDefault();
        documentTable.ajax.reload();
    });


});

function deleteDocument(documentId) {
    return deleteItem(documentUrlPage + '/' + documentId, documentTable);
}

function updateIsReviewed(id, isReviewed) {
    isReviewed = parseInt(isReviewed) === 1 ? 2 : 1;
    console.log('isReviewed',isReviewed);
    var data = {
        is_reviewed: isReviewed
    };
    toggleAction(documentUrlPage + '/' + id + '/update-reviewed', data, documentTable);
}

