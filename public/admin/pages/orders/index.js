var orderTable;
$(function () {
    var orderTableId = $('#orderTable'), status = $('#status'), adminId = $('#adminId');
    orderTable = orderTableId.DataTable({
        ajax: {
            url: orderUrlJsonData,
            data: function (data) {
                data.status = status.val();
                data.admin_id = adminId.val();
            }
        },
        processing: true,
        serverSide: true,
        order: [[0, "desc"]],
        columns: [
            {data: 'id', title: 'ID', name: 'id'},
            {data: 'user.name', title: 'Họ tên', name: 'user.name'},
            {data: 'user.email', title: 'Email', name: 'user.email'},
            {data: 'user.phone_number', title: 'Số điện thoại', name: 'user.phone_number'},
            {
                data: 'status_name',
                title: 'Trạng thái',
                name: 'status_name',
                sortable: false,
                searchable: false,
                render: function (data, type, row, meta) {
                    var status = parseInt(row.status);
                    var checked = status === 1 ? 'checked' : '';

                    return "<div class=\"demo-switch\">" +
                        "                                <div class=\"switch\">" +
                        "                                    <label>Chưa thanh toán<input onchange='javascript: toggleStatus(" + row.id + ',' + row.status + ',"' + row.user.email + "\")' class=\"chk-col-deep-purple\" type=\"checkbox\" " + checked + "><span class=\"lever\"></span>Đã thanh toán</label>" +
                        "                                </div>" +
                        "                            </div>";

                }
            },
            {
                data: 'censorship_name',
                title: 'Người duyệt',
                name: 'censorship_name',
                searchable: false,
                sortable: false
            },
            {data: 'status', title: 'Trạng thái', name: 'status', visible: false},
            {
                data: 'course_ids_as_array', title: 'Xem danh sách khoá học', name: 'course_ids_as_array', sortable: false, searchable: false,
                render: function (data, type, row, meta) {
                    return '<button onclick="javascript: showDetailOrder(' + row.id + ')" data-type="html-message" class="btn bg-amber waves-effect"><i class="material-icons">remove_red_eye</i><span class="icon-name">Chi tiết</span></button>';
                }
            },
            {data: 'created_at', title: 'Ngày tạo', name: 'created_at'},

        ]
    });
    status.add(adminId).on('change', function (e) {
        e.preventDefault();
        orderTable.ajax.reload();
    });
});

function toggleStatus(id, status, email) {
    status = status === 1 ? 2 : 1;
    var data = {
        status: status,
        email: email
    };
    toggleAction(orderUrlPage + '/' + id, data, orderTable);
}

function showDetailOrder(id) {
    $.ajax({
        method: 'GET',
        url: orderUrlPage + '/' + id
    }).success(function (response) {
        if (response.data.length === 0) {
            var html = 'Chưa chọn khoá học';
        } else {
            html = ` <ul class="list-group" style="text-align: left">`;
            response.data.forEach((course) => {
                html += `<li class="list-group-item">${course.name}<span class="badge bg-pink">${course.pretty_price}</span></li>`;
            });


            html += `</ul>`;
        }
        showAlert('Chi tiết khoá học!', html);
    }).fail(function (error) {
        alert(error);
    });

}

