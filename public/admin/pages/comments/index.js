var commentTable;
$(function () {
    var commentTableId = $('#commentTable'), isReviewed = $('#isReviewed');
    commentTable = commentTableId.DataTable({
        ajax: {
            url: commentUrlJsonData,
            data: function (data) {
                data.is_reviewed = isReviewed.val();
            }
        },
        processing: true,
        serverSide: true,
        order: [[0, "desc"]],
        columns: [
            {data: 'id', title: 'ID', name: 'id'},
            {data: 'user.name', title: 'Người tạo', name: 'user.name'},
            {data: 'course.name', title: 'Khoá học', name: 'course.name'},
            {data: 'created_at', title: 'Ngày tạo', name: 'created_at'},
            {data: 'comment', title: 'Nội dung', name: 'comment'},
            {data: 'is_reviewed', title: 'Số điện thoại', name: 'is_reviewed', visible: false},
            {
                data: 'status_name',
                title: 'Trạng thái',
                name: 'status_name',
                sortable: false,
                searchable: false,
                render: function (data, type, row, meta) {
                    var status = parseInt(row.is_reviewed);
                    var checked = status === 1 ? 'checked' : '';

                    return "<div class=\"demo-switch\">" +
                        "                                <div class=\"switch\">" +
                        "                                    <label>Chưa duyệt<input onchange='javascript: toggleStatus(" + row.id + ',' + row.is_reviewed + ',"' + row.user.email + "\")' class=\"chk-col-deep-purple\" type=\"checkbox\" " + checked + "><span class=\"lever\"></span>Đã duyệt</label>" +
                        "                                </div>" +
                        "                            </div>";

                }
            }
        ]
    });
    isReviewed.on('change', function (e) {
        e.preventDefault();
        commentTable.ajax.reload();
    });
});

function toggleStatus(id, is_reviewed) {
    is_reviewed = is_reviewed === 1 ? 2 : 1;
    var data = {
        is_reviewed: is_reviewed
    };
    toggleAction(commentUrlPage + '/' + id + '/update-reviewed', data, commentTable);
}

