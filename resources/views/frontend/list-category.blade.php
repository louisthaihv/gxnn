<div class="sidebar-box ftco-animate">
    <h3 class="heading-sidebar">Đề mục</h3>
    <ul class="categories">
        @foreach($categories as $category)
        <li><a href="{{ route('category.posts', $category->id) }}">{{ $category->title }} <span>({{ $category->posts_count }})</span></a></li>
        @endforeach
    </ul>
</div>