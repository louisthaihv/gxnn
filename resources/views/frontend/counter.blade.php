<section class="ftco-counter" id="section-counter">
    <div class="container-fluid px-0">
        <div class="row no-gutters">
            <div class="col-md-3 justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center py-5">
                    <div class="text">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-users"></span>
                        </div>
                        <strong class="number" data-number="98087">0</strong>
                        <span>Giáo dân</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center py-5">
                    <div class="text">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-user"></span>
                        </div>
                        <strong class="number" data-number="10">0</strong>
                        <span>Linh mục</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center py-5">
                    <div class="text">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-group"></span>
                        </div>
                        <strong class="number" data-number="10">0</strong>
                        <span>Hội đoàn</span>
                    </div>
                </div>
            </div>
            <div class="col-md-3 justify-content-center counter-wrap ftco-animate">
                <div class="block-18 text-center py-5">
                    <div class="text">
                        <div class="icon d-flex justify-content-center align-items-center">
                            <span class="icon-home"></span>
                        </div>
                        <strong class="number" data-number="206">0</strong>
                        <span>Gia đình</span>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>