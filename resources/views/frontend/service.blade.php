<section class="ftco-section ftco-services-2" id="services-section">
    <div class="container">
        <div class="row justify-content-center pb-5">
            <div class="col-md-12 heading-section text-center ftco-animate">
                <span class="subheading"><a target="_blank" href="{{ route('category.posts', $activity->id) }}" class="grayLink">{{ $activity->title }}</a></span>
                <h2 class="mb-4">{{ $activity->description }}</h2>
                <p>{{ $activity->content }}</p>
            </div>
        </div>
        <div class="row">
            @foreach($activity->posts as $post)
            <div class="col-md d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services text-center d-block">
                    <div class="icon">
                        <a target="_blank" href="{{ route('post.slug', $post->slug) }}">
                            <img class="img-responsive img-circle" width="60" src="{{ $post->getImageUrl() }}" >
                        </a>
                    </div>
                    <div class="media-body">
                        <a target="_blank" href="{{ route('post.slug', $post->slug) }}"><h3 class="heading mb-3">{{ $post->title }}</h3></a>
                        <p>
                            {!! $post->description !!}
                        </p>
                    </div>
                </div>
            </div>
            @endforeach
            <!-- <div class="col-md d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services text-center d-block">
                    <div class="icon"><span class="flaticon-bible"></span></div>
                    <div class="media-body">
                        <h3 class="heading mb-3">Continous Teaching</h3>
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                    </div>
                </div>
            </div>
            <div class="col-md d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services text-center d-block">
                    <div class="icon"><span class="flaticon-church"></span></div>
                    <div class="media-body">
                        <h3 class="heading mb-3">Set of Sermons</h3>
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                    </div>
                </div>
            </div>
            <div class="col-md d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services text-center d-block">
                    <div class="icon"><span class="flaticon-rings"></span></div>
                    <div class="media-body">
                        <h3 class="heading mb-3">Wedding</h3>
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                    </div>
                </div>
            </div>
            <div class="col-md d-flex align-self-stretch ftco-animate">
                <div class="media block-6 services text-center d-block">
                    <div class="icon"><span class="flaticon-social-care"></span></div>
                    <div class="media-body">
                        <h3 class="heading mb-3">Community Helpers</h3>
                        <p>A small river named Duden flows by their place and supplies it with the necessary regelialia.</p>
                    </div>
                </div>
            </div> -->
        </div>
    </div>
</section>