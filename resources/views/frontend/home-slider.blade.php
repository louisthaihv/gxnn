<section class="home-slider js-fullheight owl-carousel">
  @foreach($homeSlide->posts as $post)
  <div class="slider-item js-fullheight" style="background-image:url('{{ $post->getImageUrl() }}');">
    <div class="overlay"></div>
    <div class="container">
      <div class="row no-gutters slider-text js-fullheight align-items-center justify-content-center" data-scrollax-parent="true">
        <div class="col-md-8 text-center ftco-animate mt-5">
          <div class="text">
            <div class="subheading">
              <span>{{ $post->title }}</span>
            </div>
            <h1 class="mb-4">{!! $post->description !!}</h1>
            <p>{!! $post->getLimitContent() !!}</p>
            <p>
              {{--  <a href="#" class="btn btn-primary py-2 px-4">Be part of us</a>  --}}
              <a target="_blank" href="{{ route('post.slug', $post->slug) }}" class="btn btn-primary btn-outline-primary py-2 px-4">
                Xem chi tiết
              </a>
            </p>
          </div>
        </div>
      </div>
    </div>
  </div>
  @endforeach
</section>