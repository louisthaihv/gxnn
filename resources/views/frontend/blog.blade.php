<section class="ftco-section bg-light" id="blog-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-5">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <span class="subheading"><a target="_blank" href="{{ route('category.posts', $blog->id) }}" class="grayLink">{{ $blog->title }}</a></span>
                <h2 class="mb-4">{{ $blog->description }}</h2>
                <p>{{ $blog->content }}</p>
            </div>
        </div>
        <div class="row d-flex">
            @foreach($blog->posts as $post)
            <div class="col-md-4 d-flex ftco-animate">
                <div class="blog-entry justify-content-end">
                    <a href="single.html" class="block-20" style="background-image: url('{{ asset($post->getImageUrl()) }}');">
                    </a>
                    <div class="text float-right d-block">
                        <div class="d-flex align-items-center pt-2 mb-4 topp">
                            <div class="one mr-2">
                                <span class="day">{{ $post->getDay() }}</span>
                            </div>
                            <div class="two">
                                <span class="yr">{{ $post->getYear() }}</span>
                                <span class="mos">{{ $post->getMonth() }}</span>
                            </div>
                        </div>
                        <h3 class="heading">
                            <a target="_blank" href="{{ route('post.slug', $post->slug) }}">{{ $post->title }}</a>
                        </h3>
                        <p>
                            {{ $post->description }}
                        </p>
                        <div class="d-flex align-items-center mt-4 meta">
                            <p class="mb-0"><a href="{{ route('post.slug', $post->slug) }}" class="btn btn-primary">Xem chi tiết <span class="ion-ios-arrow-round-forward"></span></a></p>
                            <p class="ml-auto mb-0">
                                <a href="#" class="mr-2">&nbsp;{{ $post->user->name }}</a>
                                <a href="#" class="meta-chat"><span class="icon-chat"></span> 3</a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>