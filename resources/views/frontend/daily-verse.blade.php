<section class="ftco-daily-verse img" style="background-image: url('{{ $dailyVerse->getImageUrl() }}');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-10">
                <div class="row justify-content-center align-items-center">
                    <div class="col-md-2 ftco-animate">
                        <div class="icon">
                            <span class="flaticon-church"></span>
                        </div>
                    </div>
                    <div class="col-md-10 daily-verse pl-md-5 ftco-animate">
                            <a href="{{ route('post.slug', $dailyVerse->id) }}" target="_blank"><span class="subheading">{{ $dailyVerse->title }}</span></a>
                        <h3>{!! $dailyVerse->content !!}</h3>
                        <h4 class="h5 mt-4 font-weight-bold">{{ $dailyVerse->description }}</h4>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>