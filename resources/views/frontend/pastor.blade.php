<section class="ftco-section ftco-no-pb" id="pastor-section">
    <div class="container">
        <div class="row justify-content-center pb-5">
            <div class="col-md-6 heading-section text-center ftco-animate">
            <span class="subheading">{{ $pastor->title }}</span>
                <h2 class="mb-4">{{ $pastor->description }}</h2>
                <p>{{ $pastor->content }}</p>
            </div>
        </div>
        <div class="row">
            @foreach($pastor->posts as $post)
            <div class="col-md-6 col-lg-3 ftco-animate">
                <div class="staff">
                    <div class="img-wrap d-flex align-items-stretch">
                    <div class="img align-self-stretch" style="background-image: url('{{ $post->getImageUrl() }}');"></div>
                    </div>
                    <div class="text d-flex align-items-center pt-3 text-center">
                        <div>
                        <a target="_blank" href="{{ route('post.slug', $post->slug) }}"><h3 class="mb-2">{{ $post->title }}</h3></a>
                            <span class="position mb-4">{{ $post->description }}</span>
                            <div class="faded">
                                <ul class="ftco-social text-center">
                                    <li class="ftco-animate"><a href="#"><span class="icon-twitter"></span></a></li>
                                    <li class="ftco-animate"><a href="#"><span class="icon-facebook"></span></a></li>
                                    <li class="ftco-animate"><a href="#"><span class="icon-google-plus"></span></a></li>
                                    <li class="ftco-animate"><a href="#"><span class="icon-instagram"></span></a></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>