<section class="ftco-section ftco-no-pt ftco-no-pb ftco-about-section" id="about-section">
    <div class="container-fluid px-0">
        <div class="row d-md-flex text-wrapper">
            <div class="one-half img" style="background-image: url('{{ asset($aboutPost->getImageUrl()) }}');"></div>
            <div class="one-half half-text d-flex justify-content-end align-items-center ftco-animate">
                <div class="text-inner pl-md-5">
                    <h3 class="heading">{{ $aboutPost->title }}</h3>
                    <p>
                        {!! $aboutPost->content !!}
                    </p>
                    <ul class="my-4">
                        <li><span class="ion-ios-checkmark-circle mr-2"></span> Tập thể duy nhất</li>
                        <li><span class="ion-ios-checkmark-circle mr-2"></span> Sống Thánh Thiện</li>
                        <li><span class="ion-ios-checkmark-circle mr-2"></span> Công giáo & Tông Truyền</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</section>