@extends('frontend.master')
@section('content')
<section class="hero-wrap hero-wrap-2" style="background-image: url('{{ asset($viewData['category']->getImageUrl()) }}');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate pb-5 text-center">
            <p class="breadcrumbs">
                <span class="mr-2"><a href="{{ route('index') }}">Trang Chủ <i class="ion-ios-arrow-forward"></i></a></span>
                <span class="mr-2"><a href="{{ route('category.posts', $viewData['category']->id) }}">{{ $viewData['category']->title }} <i class="ion-ios-arrow-forward"></i></a></span>
                <span>Danh sách bài viết<i class="ion-ios-arrow-forward"></i></span>
            </p>
            </div>
        </div>
        </div>
    </section>
    <section class="ftco-section">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 ftco-animate">
                    {{-- {{ dd($viewData['posts']) }} --}}
                </div>

                <div class="col-lg-4 sidebar ftco-animate">

                    @include('frontend.search-form')
                </div>

            </div>
        </div>
    </section>    
@stop