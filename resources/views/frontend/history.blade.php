@extends('frontend.master')
@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate pb-5 text-center">
            <h1 class="mb-3 bread">Lịch Sử</span></h1>
            <p class="breadcrumbs">
                <span class="mr-2"><a href="{{ route('index') }}">Trang Chủ <i class="ion-ios-arrow-forward"></i></a></span> 
                <span>Lịch Sử<i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
        </div>
    </section>
    <section class="ftco-section">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 ftco-animate">
                    Đây là lịch sử
                </div>

            </div>
        </div>
    </section>    
@stop