<section class="ftco-section bg-light" id="sermons-section">
    <div class="container">
        <div class="row justify-content-center pb-5">
            <div class="col-md-12 heading-section text-center ftco-animate">
            <span class="subheading"><a target="_blank" href="{{ route('category.posts', $sermon->id) }}" class="grayLink">{{ $sermon->title }}</a></span>
                <h2 class="mb-4">{{ $sermon->description }}</h2>
                <p>
                    {!! $sermon->description !!}
                </p>
            </div>
        </div>
        <div class="row">
            @foreach ($sermon->posts as $post)
            <div class="col-md-4">
                <div class="sermon-wrap ftco-animate">
                    <div class="img d-flex align-items-center justify-content-center" style="background-image: url('{{ asset($post->getImageUrl()) }}');">
                        <div class="text-content p-4 text-center">
                            <span>Linh Mục:</span>
                            <h3>Admin</h3>
                            <p class="">
                                <a target="_blank" href="{{ $post->source }}" class="btn-custom mb-2 p-2 text-center popup-vimeo"><span class="icon-play"></span> Xem</a>
                                {{-- <a href="{{ route('post.slug', $post->slug) }}" target="_blank" class="btn-custom p-2 text-center"> Chi tiết</a> --}}
                            </p>
                        </div>
                    </div>
                    <div class="text pt-3 text-center">
                        <h2 class="mb-0">
                            <a href="{{ route('post.slug', $post->title) }}">
                                {{ $post->title }}
                            </a>
                        </h2>
                        <div class="meta">
                            <p class="mb-0">
                                <span>{{ $post->created_at->format('d/m/Y') }}</span>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>