<section class="ftco-gallery ftco-section ftco-no-pb mb-4">
    <div class="container-fluid px-4">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section ftco-animate text-center">
                <span class="subheading"><a target="_blank" href="{{ route('category.posts', $gallery->id) }}" class="grayLink">{{ $gallery->title }}</a></span>
                <h2 class="mb-1">{{ $gallery->description }}</h2>
            </div>
        </div>
        <div class="row">
            @foreach ($gallery->posts as $post)
            <div class="col-md-3 ftco-animate">
                <a href="{{ asset($post->getImageUrl()) }}" class="gallery image-popup img d-flex align-items-center" style="background-image: url('{{ asset($post->getImageUrl()) }}');">
                    <div class="icon mb-4 d-flex align-items-center justify-content-center">
                        <span class="icon-instagram"></span>
                    </div>
                </a>
            </div>
            @endforeach
        </div>
    </div>
</section>