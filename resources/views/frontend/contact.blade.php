<section class="ftco-section contact-section ftco-no-pb" id="contact-section">
    <div class="container">
        <div class="row justify-content-center mb-5 pb-3">
            <div class="col-md-7 heading-section text-center ftco-animate">
                <span class="subheading">Liên Hệ</span>
                <h2 class="mb-4">Liên lạc với chúng tôi</h2>
                <p>Rất mong sự đóng góp của quý cộng đoàn, giúp chúng tôi hoàn thiện hơn</p>
            </div>
        </div>

        <div class="row block-9">
            <div class="col-md-7 order-md-last d-flex">
                <form action="{{ route('email.send') }}" class="bg-light p-4 p-md-5 contact-form" id="contactForm">
                    <div class="form-group">
                        <input type="text" class="form-control" name="name" required placeholder="Tên">
                    </div>
                    <div class="form-group">
                        <input type="email" class="form-control" required name="phone" placeholder="Email">
                    </div>
                    <div class="form-group">
                        <input type="text" name="subject" class="form-control" required  placeholder="Chủ đề">
                    </div>
                    <div class="form-group">
                        <textarea name="content" id="" cols="30" rows="7" required class="form-control" placeholder="Nội dung"></textarea>
                    </div>
                    <div class="form-group">
                        <input type="submit" value="Gửi" class="btn btn-primary py-3 px-5">
                    </div>
                </form>

            </div>

            <div class="col-md-5 d-flex">
                <div class="row d-flex contact-info mb-5">
                    <div class="col-md-12 ftco-animate">
                        <div class="box p-2 px-3 bg-light d-flex">
                            <div class="icon mr-3">
                                <span class="icon-map-signs"></span>
                            </div>
                            <div>
                                <h3 class="mb-3">Địa Chỉ</h3>
                                <p>Nhà thờ Nhân Nghĩa - Thôn Nhân Nghĩa - Xã Nam Đồng - Thành phố Hải Dương - Tỉnh Hải Dương</p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 ftco-animate">
                        <div class="box p-2 px-3 bg-light d-flex">
                            <div class="icon mr-3">
                                <span class="icon-phone2"></span>
                            </div>
                            <div>
                                <h3 class="mb-3">Điện thoại liên hệ</h3>
                                <p><a href="tel://099999999">099999999</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 ftco-animate">
                        <div class="box p-2 px-3 bg-light d-flex">
                            <div class="icon mr-3">
                                <span class="icon-paper-plane"></span>
                            </div>
                            <div>
                                <h3 class="mb-3">Địa chỉ Email</h3>
                                <p><a href="mailto:gxnhannghia@gmail.com">gxnhannghia@gmail.com</a></p>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-12 ftco-animate">
                        <div class="box p-2 px-3 bg-light d-flex">
                            <div class="icon mr-3">
                                <span class="icon-globe"></span>
                            </div>
                            <div>
                                <h3 class="mb-3">Website</h3>
                                <p><a href="{{ url('/') }}">{{ url('/') }}</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>