<div class="sidebar-box ftco-animate">
    <h3 class="heading-sidebar">Bài viết liên quan</h3>
    @foreach($relatePosts as $post)
    <div class="block-21 mb-4 d-flex">
        <a class="blog-img mr-4" style="background-image: url('{{ $post->getImageUrl() }}');"></a>
        <div class="text">
            <h3 class="heading"><a href="{{ route('post.slug', $post->slug) }}">{{ $post->getLimitContent() }}</a></h3>
            <div class="meta">
            <div><a href="{{ route('post.slug', $post->slug) }}"><span class="icon-calendar"></span>{{ $post->created_at->format('d/m/y') }}</a></div>
            <div><a href="#"><span class="icon-person"></span> {{ $post->user->name }}</a></div>
            <div><a href="#"><span class="icon-chat"></span> 19</a></div>
            </div>
        </div>
    </div>
    @endforeach
</div>