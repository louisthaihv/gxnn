  @extends('frontend.master')
    @section('content')
      @include('frontend.home-slider', ['homeSlide' => $viewData['home_slide']])

      @include('frontend.about', ['aboutPost' => $viewData['about']])
    
      <!-- @include('frontend.counter') -->
      
      @include('frontend.service', ['activity' => $viewData['activity']])

      @include('frontend.sermons', ['sermon' => $viewData['sermon']])
      
      @include('frontend.event', ['event' => $viewData['event']])
      
      {{-- @include('frontend.cause') --}}

      @include('frontend.daily-verse', ['dailyVerse' => $viewData['dailyVerse']])

      @include('frontend.blog', ['blog' => $viewData['blog']])

      @include('frontend.pastor', ['pastor' => $viewData['pastor']])

      @include('frontend.contact')
      
      @include('frontend.gmap')

      @include('frontend.gallery', ['gallery' => $viewData['gallery']])
  @stop