@extends('frontend.master')
@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate pb-5 text-center">
            <p class="breadcrumbs">
                <span class="mr-2"><a href="{{ route('index') }}">Trang Chủ <i class="ion-ios-arrow-forward"></i></a></span>
                 <span>Tìm kiếm<i class="ion-ios-arrow-forward"></i></span>
            </p>
            </div>
        </div>
        </div>
    </section>
    <section class="ftco-section">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 ftco-animate">
                    <div class="widget widget-search-post">
                        <div class="widget-content">
                            <form action="{{ route('post.search') }}" class="search-post-form">
                                <div class="form-group d-flex">
                                    <div class="el-input">
                                        <input name="search" type="text" class="form-control" placeholder="Gõ từ khoá rồi nhấn enter.">
                                    </div>
                                    <button type="submit"><i class="fa fa-search"></i>Tìm kiếm</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="widget widget-list-post-search">
                        <div class="widget-content">
                            @foreach($posts as $post)
                                {{--{{ dd($posts) }}--}}
                                <div class="entry-item clearfix">
                                    <div class="entry-thumb">
                                        <a href="#"><img src="https://dummyimage.com/400x400/000/fff" alt=""></a>
                                    </div>
                                    <div class="entry-content">
                                        <div class="meta-date">
                                            <span class="user-name">Vu Van Ky</span>
                                            <span class="meta-created-at">
                                                @php
                                                    $t = strtotime($post->created_at);
                                                    echo date('M dS, Y H:i',$t);
                                                @endphp
                                            </span>
                                        </div>
                                        <h3 class="title"><a href="#">{{ $post->title }}</a></h3>
                                        <span></span>
                                        <p>{{ $post->description }}</p>
                                        <div class="post-stats">
                                        <span class="stats-item mr-05" data-tippy="" data-original-title="Views">
                                            <i aria-hidden="true" class="fa fa-eye"></i> 567
                                        </span>
                                            <span class="stats-item mx-05" data-tippy="" data-original-title="Clips">
                                            <i aria-hidden="true" class="fa fa-paperclip"></i> 0
                                        </span>
                                            <span class="stats-item mx-05" data-tippy="" data-original-title="Comments">
                                            <i aria-hidden="true" class="fa fa-comments"></i> 1
                                        </span>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>
                    <div class="widget-paginate">
                        {{ $posts->links() }}
                    </div>
                </div>

                <div class="col-lg-4 sidebar ftco-animate">

                    @include('frontend.search-form')
                </div>

            </div>
        </div>
    </section>    
@stop