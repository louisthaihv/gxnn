@extends('frontend.master')
@section('content')
    <section class="hero-wrap hero-wrap-2" style="background-image: url('{{ $viewData['post']->getImageUrl() }}');" data-stellar-background-ratio="0.5">
        <div class="overlay"></div>
        <div class="container">
        <div class="row no-gutters slider-text align-items-end justify-content-center">
            <div class="col-md-9 ftco-animate pb-5 text-center">
            <h1 class="mb-3 bread">{{ $viewData['post']->title }}</span></h1>
            <p class="breadcrumbs"><span class="mr-2"><a href="{{ route('index') }}">Trang Chủ <i class="ion-ios-arrow-forward"></i></a></span> <span class="mr-2"><a href="{{ route('category.posts', $viewData['post']->category->id) }}">{{ $viewData['post']->category->title }} <i class="ion-ios-arrow-forward"></i></a></span> <span>{{ $viewData['post']->title }}<i class="ion-ios-arrow-forward"></i></span></p>
            </div>
        </div>
        </div>
    </section>
    <section class="ftco-section">
        <div class="container">
            <div class="row">

                <div class="col-lg-8 ftco-animate">
                    {!! $viewData['post']->content !!}
                    <div class="pt-5 mt-5">
                        <div class="comment-form-wrap pt-5">
                            <h3 class="mb-5">Để lại bình luận</h3>
                            comment
                        </div>
                    </div>
                </div>

                <div class="col-lg-4 sidebar ftco-animate">

                    @include('frontend.search-form')

                    @include('frontend.list-category', ['categories' => $viewData['categories']])
        
                    @include('frontend.relate-post', ['relatePosts' => $viewData['relatePosts']->posts])

                </div>

            </div>
        </div>
    </section>    
@stop