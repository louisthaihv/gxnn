<section class="ftco-intro img" id="events-section" style="background-image: url('{{ $event->getImageUrl() }}');">
    <div class="overlay"></div>
    <div class="container">
        <div class="row">
        </div>
    </div>
</section>

<section class="ftco-section bg-light ftco-event" id="events-section">
    <div class="container-fluid px-4 ftco-to-top">
        <div class="row justify-content-center pb-5">
            <div class="col-md-12 heading-section text-center ftco-animate">
                <span class="subheading"><a target="_blank" href="{{ route('category.posts', $event->id) }}" class="whiteLink">{{ $event->title }}</a></span>
                <h2 class="mb-5">{{ $event->description }}</h2>
            </div>
        </div>
        <div class="row">
            @foreach($event->posts as $post)
            <div class="col-md-12 col-lg-6 col-xl-4">
                <div class="event-wrap d-flex ftco-animate">
                    <div class="img" style="background-image: url('{{$post->getImageUrl()}}');"></div>
                    <div class="text p-4 d-flex align-items-center">
                        <div>
                            <span class="time">{{ $post->getPublished() }} - {{$post->getExpired()}}</span>
                            <h3><a target="_blank" href="{{ route('post.slug', $post->slug) }}">{{ $post->title }}</a></h3>
                            <div class="meta">
                                <p><span class="icon-user mr-1"></span> Người đăng: <a href="#">{{ $post->user->name }}</a></p>
                                <p><span class="icon-location"></span> {{ $post->description }}</p>
                                <p class="mb-0"><a href="{{ route('post.slug', $post->slug) }}" class="btn btn-primary">Xem chi tiết</a></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endforeach
        </div>
    </div>
</section>