<nav class="navbar navbar-expand-lg navbar-dark ftco_navbar bg-dark ftco-navbar-light site-navbar-target" id="ftco-navbar">
    <div class="container">
        <a class="navbar-brand" href="{{ route('index') }}"><span class="flaticon-bible"></span>Nhân Nghĩa</a>
        <button class="navbar-toggler js-fh5co-nav-toggle fh5co-nav-toggle" type="button" data-toggle="collapse" data-target="#ftco-nav" aria-controls="ftco-nav" aria-expanded="false" aria-label="Toggle navigation">
            <span class="oi oi-menu"></span> Menu
        </button>
        <div class="collapse navbar-collapse" id="ftco-nav">
            <ul class="navbar-nav nav ml-auto">
                <li class="nav-item"><a href="#home-section" class="nav-link"><span>{!! $homeMenu['title'] !!}</span></a></li>
                <li class="nav-item"><a href="#about-section" class="nav-link"><span>{!! $aboutMenu['title'] !!}</span></a></li>
                <!-- <li class="nav-item"><a href="#services-section" class="nav-link"><span>Hoạt động</span></a></li> -->
                <li class="nav-item"><a href="#sermons-section" class="nav-link"><span>{!! $sermonMenu['title'] !!}</span></a></li>
                <li class="nav-item"><a href="#events-section" class="nav-link"><span>{!! $eventMenu['title'] !!}</span></a></li>
                {{-- <li class="nav-item"><a href="#causes-section" class="nav-link"><span>{!! $causesMenu !!}</span></a></li> --}}
                <li class="nav-item"><a href="#blog-section" class="nav-link"><span>{!! $blogMenu['title'] !!}</span></a></li>
                <li class="nav-item"><a href="#pastor-section" class="nav-link"><span>{!! $pastorMenu['title'] !!}</span></a></li>
                <li class="nav-item"><a href="#contact-section" class="nav-link"><span>{!! $contact['title'] !!}</span></a></li>
            </ul>
        </div>
    </div>
</nav>