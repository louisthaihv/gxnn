@component('mail::message', ['headeUrl' => $headerUrl, 'headerTitle' => $headerTitle])




@component('mail::panel')

@endcomponent

@component('mail::table')
|      Tên           |        Email/Phone         |         Chủ đề       |
| -------------------|----------------------------| ---------------------|
|{{ $data['name'] }} |   {{ $data['phone'] }}     | {{$data['subject']}} |

<hr />
<b>Nội Dung:</b> <br>

{{ $data['content'] }}

@endcomponent

Thanks,<br>
@endcomponent