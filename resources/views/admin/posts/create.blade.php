@extends('admin.layouts.layouts')
@section('css')
    <link href="{{asset('admin/plugins/dropzone/dropzone.css')}}" rel="stylesheet">
    <!-- Bootstrap Material Datetime Picker Css -->
    <link href="/admin/plugins/bootstrap-material-datetimepicker/css/bootstrap-material-datetimepicker.css" rel="stylesheet" />

    <!-- Bootstrap DatePicker Css -->
    <link href="/admin/plugins/bootstrap-datepicker/css/bootstrap-datepicker.css" rel="stylesheet" />

    <style>
        .dz-image {
          width: 100% !important;
          height: 100% !important;
        }
        .dz-preview {
          width: 90% !important;
          height: 90% !important
        }
    </style>
@stop
@section('content')
<div class="container-fluid">
  <div class="block-header">
    <h2>
      Quản lý bài viết
    </h2>
  </div>
  <!-- Basic Examples -->
  <div class="row clearfix">

    <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
      <div class="card">
        <div class="header">
          <h2>
            Tạo mới bài viết
          </h2>
        </div>
        <div class="body">
          @include('admin.includes.errors')
          @include('admin.includes.success')

          {!! Form::open(['route' => 'posts.store', 'method' => 'POST', 'id' => 'frmCreatePost', 'class' => 'dropzone', 'files' => true]) !!}
          <div class="row clearfix">

            <div class="col-sm-6">
              <div class="form-group form-float">
                <div class="form-line">
                  {!! Form::select('category_id', $categoriesSelect, '', ['class' => 'form-control show-tick', 'id' => 'categoryId', 'required' => true]) !!}
                  <label class="form-label">Mục</label>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
              <div class="form-group form-float">
                <div class="form-line">
                  {!! Form::text('title', '', [
                  'class' => 'form-control',
                  'required' => true,
                  'minlength' => 10,
                  'maxlength' => 255
                  ])!!}
                  <label class="form-label">Tiêu đề</label>
                </div>
              </div>
            </div>
            <div class="col-sm-6">
                <div class="form-group form-float">
                  <div class="form-line">
                    {!! Form::select('type', config('post.post_type'), '', ['class' => 'form-control show-tick', 'id' => 'postType', 'required' => true]) !!}
                    <label class="form-label">Loại bài viết</label>
                  </div>
                </div>
              </div>
            <div class="col-sm-6">
              <div class="form-group form-float">
                <div class="form-line">
                  {!! Form::text('source', '', ['class' => 'form-control',
                  'minlength' => 5,
                  'maxlength' => 100
                  ]) !!}
                  <label class="form-label">Nguồn</label>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group form-float">
                <div class="form-line">
                  {!! Form::text('published_at', '', ['class' => 'form-control datepicker']) !!}
                  <label class="form-label">Ngày bắt đầu</label>
                </div>
              </div>
            </div>

            <div class="col-sm-6">
              <div class="form-group form-float">
                <div class="form-line">
                  {!! Form::text('expired_at', '', ['class' => 'form-control datepicker']) !!}
                  <label class="form-label">Ngày kết thúc</label>
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="form-group form-float">
                <span>Miêu tả ngắn: </span>
                <div class="form-line">
                  {!! Form::textarea('description', null, ['id' => 'post_desciption', 'required' => true]) !!}
                </div>
              </div>
            </div>

            <div class="col-sm-12">
              <div class="form-group form-float">
                <span>Nội dung: </span>
                <div class="form-line">
                  {!! Form::textarea('content', null, ['id' => 'tinymce', 'required' => true]) !!}
                </div>
              </div>
            </div>

            
            <div class="col-sm-12 ">
                <label class="form-label">Ảnh</label>
                <div class="dz-message">
                    <div class="drag-icon-cph">
                        <i class="material-icons">touch_app</i>
                    </div>
                    <h3>Kéo thả hoặc click để upload ảnh</h3>
                    <em>(Chọn ảnh cho bài viết)</em>
                </div>
                <div class="fallback">
                    <input name="image" type="file" required/>
                </div>
            </div>

            <div class="col-sm-12">
              <button type="button" class="btn btn-primary btn-lg m-l-15 waves-effect" id='createBtn'>Cập nhật</button>
            </div>
          </div>
          {!! Form::close() !!}
        </div>
      </div>
    </div>
  </div>
  <!-- #END# Basic Examples -->

</div>
@stop

@section('js')
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/admin/plugins/dropzone/dropzone.js"></script>
    <script src="/admin/plugins/tinymce/tinymce.min.js"></script>
    <script src="/admin/js/pages/forms/editors.js"></script>
    <!-- Moment Plugin Js -->
    <script src="/admin/plugins/momentjs/moment.js"></script>

    <!-- Bootstrap Material Datetime Picker Plugin Js -->
    <script src="/admin/plugins/bootstrap-material-datetimepicker/js/bootstrap-material-datetimepicker.js"></script>

    <!-- Bootstrap Datepicker Plugin Js -->
    <script src="/admin/plugins/bootstrap-datepicker/js/bootstrap-datepicker.js"></script>
    <script>
        var createPost = $('#frmCreatePost');

        $(function() {
          $('.datepicker').bootstrapMaterialDatePicker({
              format: 'DD-MM-YYYY',
              clearButton: true,
              weekStart: 1,
              time: false
          });

            createPost.validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });
        
        
            Dropzone.options.frmCreatePost = {
                paramName: "image",
                maxFilesize: 20,
                maxFiles: 1,
                uploadMultiple: false,
                addRemoveLinks: true,
                acceptedFiles: '.jpg, .jpeg, .gif, .png',
                dictRemoveFile: 'Xoá file',
                autoProcessQueue: false,
                init: function () {
                    var submitButton = document.querySelector("#createBtn");
                    var myDropzone = this;
                    submitButton.addEventListener("click", function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        if(createPost.valid()){
                            myDropzone.processQueue();
                        }
                    });
                    this.on("thumbnail", function(file, dataUrl) {
                        $('.dz-image').last().find('img').attr({width: '100%', height: '100%'});
                    }),
                    this.on('sending', function (file, xhr, formData) {
                        var data = $('form').serializeArray();
                        tinymce.triggerSave();
                        $.each(data, function (key, el) {
                            formData.append(el.name, el.value);
                        });
                    });
                    this.on('success', function (data, response) {
                        $('.dz-image').css({"width":"100%", "height":"auto"});
                        toastr.clear();
                        if (response.code == 200) {
                            toastr.success(response.message, 'Thông Báo');
                            setTimeout(function(){
                                window.location.href = response.data.url;
                            }, 1000);
                        } else {
                            toastr.error(response.message, 'Thông Báo');
                            setTimeout(function(){
                                window.location.reload();
                            }, 1000);
                        }
                    });
        
                    this.on('error', function (file, response) {
                        var errorsMsg = '<ul class="list-group error-msg">';
        
                        if(response.code === 500) {
                            errorsMsg += '<li style="border-bottom: 2px solid #fff" class="list-group-item list-group-item-danger">';
                            errorsMsg += 'Đã xảy ra lỗi trong thao tác, vui lòng thử lại';
                            errorsMsg += '</li>';
                        } else{
                            for (var i in response.errors) {
                                errorsMsg += '<li style="border-bottom: 2px solid #fff" class="list-group-item list-group-item-danger">';
                                errorsMsg += response.errors[i].shift();
                                errorsMsg += '</li>';
                            }
                        }
                        errorsMsg += '</ul>';
        
                        $('.body').find('.error-msg').remove();
                        $('.body').prepend(errorsMsg);
        
                    });
                    this.on('maxfilesexceeded', function(file) {
                        myDropzone.removeFile( file );
                    })
                }
            };
        
        });
            
    </script>
@stop
