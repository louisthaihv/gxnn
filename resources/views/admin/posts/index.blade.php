@extends('admin.layouts.table')


@section('table')
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                Quản lý bài viết
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="card">
                    <div class="header">
                        <h2>
                            Danh sách bài posts
                        </h2>
                    </div>
                    <div class="body">
                        <div class="row clearfix">
                            <div class="col-sm-3">
                                <h5 class="card-inside-title">Tác giả:</h5>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::select('user_id', $allUser, '', ['class' => 'form-control show-tick', 'id' => 'userId', 'data-live-search' => 'true']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <h5 class="card-inside-title">Loại bài viết:</h5>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::select('type', array_replace(['' => 'Tất cả'], config('post.post_type')), '', ['class' => 'form-control show-tick', 'id' => 'postType']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-3">
                                <h5 class="card-inside-title">Trạng thái:</h5>
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::select('active', array_replace(['' => 'Tất cả'], config('post.post_status')), '', ['class' => 'form-control show-tick', 'id' => 'postType']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-5 text-right">
                                <a href="{{ route('posts.create') }}" class="btn">Tạo mới bài viết</a>
                            </div>
                        </div>
                        @include('admin.includes.success')
                        <div class="table-responsive">

                            <table id="postTable"
                                   class="table table-bordered table-striped table-hover js-basic-example dataTable"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('javascript')
    <script>
        var postUrlJsonData = "{{route('admin.posts.as.json')}}";
        var postUrlPage = "{{route('posts.index')}}";
        var userUrlPage = "{{route('users.index')}}";
        var categoryUrlPage = "{{route('categories.index')}}";
    </script>
    <script src="/admin/js/pages/posts/index.js"></script>
@stop
