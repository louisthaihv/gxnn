@extends('admin.layouts.table')


@section('table')
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                Quản lý đề mục
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">

                <div class="card">
                    <div class="header">
                        <h2>
                            Danh sách đề mục
                        </h2>
                    </div>
                    <div class="body">
                        {{--  <div class="row clearfix">
                            <div class="col-sm-3">
                                <a href="{{ route('categories.create') }}" class="btn">Tạo mới đề mục</a>
                            </div>
                        </div>  --}}
                        @include('admin.includes.success')
                        <div class="table-responsive">

                            <table id="categoryTable"
                                   class="table table-bordered table-striped table-hover js-basic-example dataTable"></table>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>
@stop

@section('javascript')
    <script>
        var categoryUrlJsonData = "{{route('admin.categories.as.json')}}";
        var categoryUrlPage = "{{route('categories.index')}}";
    </script>
    <script src="/admin/js/pages/categories/index.js"></script>
@stop
