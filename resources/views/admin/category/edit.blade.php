@extends('admin.layouts.layouts')
@section('css')
    <link href="/admin/plugins/dropzone/dropzone.css" rel="stylesheet" />
@stop
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                Quản lý thành đề mục
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Cập nhật đề mục : {{$category->title}}
                        </h2>
                    </div>
                    <div class="body">
                        @include('admin.includes.errors')
                        @include('admin.includes.success')

                        {!! Form::open(['route' => ['categories.update', $category->id], 'files'=>true, 'method' => 'PUT', 'id' => 'editCategory', 'class' => 'dropzone']) !!}
                        <div class="row clearfix">
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('title', $category->title, [
                                            'class' => 'form-control',
                                            'required' => true,
                                            'minlength' => 5,
                                            'maxlength' => 100
                                         ])!!}
                                        <label class="form-label">Tiêu Đề</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Mô tả (<span class="col-pink">*</span>)</label>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::textarea('description', $category->description, ['id' => 'category_desc', 'required' => true, 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                                    <div class="form-group form-float">
                                        <div class="form-line">
                                            <label class="form-label">Nội dung (<span class="col-pink">*</span>)</label>
                                        </div>
                                    </div>
                                </div>
                            <div class="col-sm-12">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::textarea('content', $category->content, ['id' => 'tinymce', 'required' => true, 'class' => 'form-control']) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-12 ">
                                <div class="dz-message">
                                    <div class="drag-icon-cph">
                                        <i class="material-icons">touch_app</i>
                                    </div>
                                    <h3>Kéo thả hoặc click để upload ảnh</h3>
                                    <em>(Chọn ảnh cho đề mục)</em>
                                </div>
                                <div class="fallback">
                                    <input name="image" type="file"/>
                                </div>
                            </div>
                            <div class="clearfix">
                                <div class="col-sm-12">
                                    <button type="button" class="btn btn-primary btn-lg m-l-15 waves-effect" id='createBtn'>Cập nhật</button>
                                </div>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

    </div>
@stop

@section('js')
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>
    {{--  <script src="/admin/pages/users/validate.js"></script>  --}}
    <script src="/admin/plugins/tinymce/tinymce.js"></script>
    <script src="/admin/js/pages/forms/editors.js"></script>
    <script src="/admin/plugins/dropzone/dropzone.js"></script>
    <script>
        var images = [];
        var name = "{{ $category->getImageUrl() }}";
        var categoryId = "{{ $category->id }}";
        urlDeleteCategory = '';
        images.push({
            name: name, size: 321, id: categoryId
        });
        var editCategory = $('#editCategory');
        
        $(function() {
            editCategory.validate({
                highlight: function (input) {
                    $(input).parents('.form-line').addClass('error');
                },
                unhighlight: function (input) {
                    $(input).parents('.form-line').removeClass('error');
                },
                errorPlacement: function (error, element) {
                    $(element).parents('.form-group').append(error);
                }
            });
            
            Dropzone.options.editCategory = {
                paramName: "image",
                maxFilesize: 10,
                maxFiles: 5,
                uploadMultiple: true,
                addRemoveLinks: true,
                acceptedFiles: '.jpg, .jpeg, .png',
                dictRemoveFile: 'Xoá file',
                autoProcessQueue: false,
                
                init: function () {

                    var submitButton = document.querySelector("#createBtn");
                    var myDropzone = this;
                    
                    images.forEach(function(image) {
                        myDropzone.options.addedfile.call(myDropzone, image);
                        myDropzone.options.thumbnail.call(myDropzone, image, image.name);
        
                    });
        
                    submitButton.addEventListener("click", function (e) {
                        e.preventDefault();
                        e.stopPropagation();
                        if(editCategory.valid()){
                            if (myDropzone.getQueuedFiles().length > 0) {                        
                                myDropzone.processQueue();  
                            } else {                       
                                editCategory.submit();
                            }    
                        }
                    });
                    this.on('sending', function (file, xhr, formData) {
                        console.log('sending');
                        var data = $('form').serializeArray();
                        $.each(data, function (key, el) {
                            formData.append(el.name, el.value);
                        });
                    });
        
                    this.on('removedfile', function(file) {
                        console.log('remove file called');
                    });
        
                    this.on('success', function () {
                        window.location.reload();
                    });
        
                    this.on('error', function (file, response) {
                        console.log('error');
                        var errorsMsg = '<ul class="list-group error-msg">';
        
                        if(response.code === 500) {
                            errorsMsg += '<li style="border-bottom: 2px solid #fff" class="list-group-item list-group-item-danger">';
                            errorsMsg += 'Đã xảy ra lỗi trong thao tác, vui lòng thử lại';
                            errorsMsg += '</li>';
                        } else{
                            for (var i in response.errors) {
                                errorsMsg += '<li style="border-bottom: 2px solid #fff" class="list-group-item list-group-item-danger">';
                                errorsMsg += response.errors[i].shift();
                                errorsMsg += '</li>';
                            }
                        }
                        errorsMsg += '</ul>';
        
                        $('.body').find('.error-msg').remove();
                        $('.body').prepend(errorsMsg);
        
                    });
                    this.on('maxfilesexceeded', function(file) {
                        myDropzone.removeFile( file );
                    })
                }
            };
        
        });
            
    </script>
@stop
