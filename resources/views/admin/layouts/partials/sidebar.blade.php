<!-- Menu -->
<div class="menu">
    <ul class="list">
        <li class="header">ADMIN NAVIGATION</li>
        <li @if(\Request::is('admin')) class="active" @endif>
            <a href="{{ route('admin.dashboard') }}">
                <i class="material-icons">home</i>
                <span>Dashboard</span>
            </a>
        </li>


        <li class="header">HỆ THỐNG</li>
        <li @if(Request::is('admin/users') || Request::is('admin/users/*')) class="active" @endif>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">group</i>
                <span>Thành viên</span>
            </a>
            <ul class="ml-menu">
                <li @if(\Request::route()->getName() === 'users.index') class="active" @endif>
                    <a href="{{ route('users.index') }}">Danh sách</a>
                </li>
                <li @if(\Request::route()->getName() === 'users.create') class="active" @endif>
                    <a href="{{ route('users.create') }}">
                        Tạo mới
                    </a>
                </li>
            </ul>
        </li>

        <li @if(Request::is('admin/posts') || Request::is('admin/posts/*')) class="active" @endif>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">library_books</i>
                <span>Bài Viết</span>
            </a>
            <ul class="ml-menu">
                <li @if(\Request::route()->getName() === 'posts.index') class="active" @endif>
                    <a href="{{ route('posts.index') }}">Danh sách</a>
                </li>
                <li @if(\Request::route()->getName() === 'posts.create') class="active" @endif>
                    <a href="{{ route('posts.create') }}">
                        Tạo mới
                    </a>
                </li>
            </ul>
        </li>

        <li @if(Request::is('admin/categories') || Request::is('admin/categories/*')) class="active" @endif>
            <a href="javascript:void(0);" class="menu-toggle">
                <i class="material-icons">library_books</i>
                <span>Đề mục</span>
            </a>
            <ul class="ml-menu">
                <li @if(\Request::route()->getName() === 'categories.index') class="active" @endif>
                    <a href="{{ route('categories.index') }}">Danh sách</a>
                </li>
                {{--  <li @if(\Request::route()->getName() === 'categories.create') class="active" @endif>
                    <a href="{{ route('categories.create') }}">
                        Tạo mới
                    </a>
                </li>  --}}
            </ul>
        </li>


    </ul>
</div>
<!-- #Menu -->
