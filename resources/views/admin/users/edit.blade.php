@extends('admin.layouts.layouts')

@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>
                Quản lý thành viên
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Cập nhật thành viên : {{$user->name}}
                        </h2>
                    </div>
                    <div class="body">
                        @include('admin.includes.errors')
                        @include('admin.includes.success')

                        {!! Form::open(['route' => ['users.update', $user->id], 'method' => 'PUT', 'id' => 'editUser']) !!}
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('name', $user->name, [
                                            'class' => 'form-control',
                                            'required' => true,
                                            'minlength' => 5,
                                            'maxlength' => 100
                                         ])!!}
                                        <label class="form-label">Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::email('email', $user->email, ['class' => 'form-control',
                                            'minlength' => 5,
                                            'maxlength' => 100
                                        ]) !!}
                                        <label class="form-label">Email</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('phone_number', $user->phone_number, ['class' => 'form-control',
                                            'minlength' => 10,
                                            'maxlength' => 11,
                                            'required' => true
                                        ]) !!}
                                        <label class="form-label">Số điện thoại</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::select('role_id', array_replace(['' => 'Nhóm người dùng'],config('role')), $user->role_id, ['class' => 'form-control show-tick', 'id' => 'roleId', 'required' => true]) !!}
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::select('is_active', array_replace(['' => 'Trạng thái'],config('user.status')), $user->is_active, ['class' => 'form-control show-tick', 'id' => 'isActive', 'required' => true]) !!}
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary btn-lg m-l-15 waves-effect" id='createBtn'>Cập nhật</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

    </div>
@stop

@section('js')
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>
    {{--  <script src="/admin/js/pages/users/validate.js"></script>  --}}
    <script src="/admin/plugins/tinymce/tinymce.js"></script>
    <script src="/admin/js/pages/forms/editors.js"></script>
@stop
