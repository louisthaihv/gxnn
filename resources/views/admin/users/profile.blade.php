@extends('admin.layouts.layouts')
@section('css')
<link href="{{asset('admin/plugins/dropzone/dropzone.css')}}" rel="stylesheet">
    <style>
        .dz-image {
          width: 100% !important;
          height: 100% !important;
        }
        .dz-preview {
          width: 90% !important;
          height: 90% !important
        }
    </style>
@stop
@section('content')
    <div class="container-fluid">
        <div class="block-header">
            <h2>
               Thông tin cá nhân
            </h2>
        </div>
        <!-- Basic Examples -->
        <div class="row clearfix">

            <div class="col-lg-12 col-md-12 col-sm-12 col-xs-12">
                <div class="card">
                    <div class="header">
                        <h2>
                            Cập nhật thông tin
                        </h2>
                    </div>
                    <div class="body">
                        @include('admin.includes.errors')

                        {!! Form::open(['route' => 'users.profile', 'method' => 'PUT', 'id' => 'editUser', 'class' => 'dropzone', 'files' => true]) !!}
                        <div class="row clearfix">
                            <div class="col-sm-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('name', $user->name, [
                                            'class' => 'form-control',
                                            'required' => true,
                                            'minlength' => 5,
                                            'maxlength' => 100
                                         ])!!}
                                        <label class="form-label">Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-4">
                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::email('email', $user->email, ['class' => 'form-control',
                                            'minlength' => 5,
                                            'maxlength' => 100
                                        ]) !!}
                                        <label class="form-label">Email</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-4">

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::text('phone_number', $user->phone_number, ['class' => 'form-control',
                                            'minlength' => 10,
                                            'maxlength' => 11,
                                            'required' => true
                                        ]) !!}
                                        <label class="form-label">Số điện thoại</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::password('password', ['class' => 'form-control', 'id' => 'password', 'minlength' => 5, 'maxlength' => 100]) !!}
                                        <label class="form-label">Mật khẩu</label>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-6">

                                <div class="form-group form-float">
                                    <div class="form-line">
                                        {!! Form::password('password_confirmation', ['class' => 'form-control', 'minlength' => 5, 'maxlength' => 100]) !!}
                                        <label class="form-label">Xác nhận mật khẩu</label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-12 ">
                                <label class="form-label">Avatar</label>
                                <div class="dz-message">
                                    <div class="drag-icon-cph">
                                        <i class="material-icons">touch_app</i>
                                    </div>
                                    <h3>Kéo thả hoặc click để upload ảnh</h3>
                                    <em>(Chọn ảnh đại diện)</em>
                                </div>
                                <div class="fallback">
                                    <input name="image" type="file" required/>
                                </div>
                            </div>
                    
                            <div class="col-sm-12">
                            <button type="button" class="btn btn-primary btn-lg m-l-15 waves-effect" id='createBtn'>Cập nhật</button>
                            </div>
                        </div>
                        {!! Form::close() !!}
                    </div>
                </div>
            </div>
        </div>
        <!-- #END# Basic Examples -->

    </div>
@stop

@section('js')
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/admin/plugins/jquery-validation/jquery.validate.js"></script>
    <script src="/admin/plugins/dropzone/dropzone.js"></script>
    <script src="/admin/js/pages/users/validate.js"></script>
    <script src="/admin/plugins/tinymce/tinymce.js"></script>
    <script src="/admin/js/pages/forms/editors.js"></script>
    <script>
            var editUser = $('#editUser');
            var images = [];
            var name = "{{ $user->getAvatar() }}";
            var userId = "{{ $user->id }}";
            if (name) {
                images.push({
                    name: name, size: 321, id: userId
                });
            }
    
            $(function() {
    
              editUser.validate({
                    highlight: function (input) {
                        $(input).parents('.form-line').addClass('error');
                    },
                    unhighlight: function (input) {
                        $(input).parents('.form-line').removeClass('error');
                    },
                    errorPlacement: function (error, element) {
                        $(element).parents('.form-group').append(error);
                    }
                });
            
            
                Dropzone.options.editUser = {
                    paramName: "image",
                    maxFilesize: 20,
                    maxFiles: 1,
                    uploadMultiple: false,
                    addRemoveLinks: true,
                    acceptedFiles: '.jpg, .jpeg, .gif, .png',
                    dictRemoveFile: 'Xoá file',
                    autoProcessQueue: false,
                    init: function () {
                        var submitButton = document.querySelector("#createBtn");
                        var myDropzone = this;
                        images.forEach(function(image) {
                            myDropzone.options.addedfile.call(myDropzone, image);
                            myDropzone.options.thumbnail.call(myDropzone, image, image.name);
            
                        });
                        submitButton.addEventListener("click", function (e) {
                            e.preventDefault();
                            e.stopPropagation();
                            if (myDropzone.getQueuedFiles().length > 0) {                        
                                    myDropzone.processQueue();  
                                } else {                       
                                    editUser.submit();
                                }    
                        });
                        this.on("thumbnail", function(file, dataUrl) {
                            $('.dz-image').last().find('img').attr({width: '100%', height: '100%'});
                        }),
                        this.on('sending', function (file, xhr, formData) {
                            var data = $('form').serializeArray();
                            tinymce.triggerSave();
                            $.each(data, function (key, el) {
                                formData.append(el.name, el.value);
                            });
                        });
                        this.on('success', function (data, response) {
                            $('.dz-image').css({"width":"100%", "height":"auto"});
                            toastr.clear();
                            if (response.code == 200) {
                                toastr.success(response.message, 'Thông Báo');
                                setTimeout(function(){
                                    //window.location.reload();
                                }, 1000);
                            } else {
                                toastr.error(response.message, 'Thông Báo');
                                setTimeout(function(){
                                    //window.location.reload();
                                }, 1000);
                            }
                        });
            
                        this.on('error', function (file, response) {
                            var errorsMsg = '<ul class="list-group error-msg">';
            
                            if(response.code === 500) {
                                errorsMsg += '<li style="border-bottom: 2px solid #fff" class="list-group-item list-group-item-danger">';
                                errorsMsg += 'Đã xảy ra lỗi trong thao tác, vui lòng thử lại';
                                errorsMsg += '</li>';
                            } else{
                                for (var i in response.errors) {
                                    errorsMsg += '<li style="border-bottom: 2px solid #fff" class="list-group-item list-group-item-danger">';
                                    errorsMsg += response.errors[i].shift();
                                    errorsMsg += '</li>';
                                }
                            }
                            errorsMsg += '</ul>';
            
                            $('.body').find('.error-msg').remove();
                            $('.body').prepend(errorsMsg);
            
                        });
                        this.on('maxfilesexceeded', function(file) {
                            myDropzone.removeFile( file );
                        })
                    }
                };
            
            });
                
        </script>
@stop
