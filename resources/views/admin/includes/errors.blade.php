@if($errors->any())
    <ul class="list-group">
        @foreach($errors->all() as $error)
            <li class="list-group-item list-group-item-danger">{{$error}}</li>
        @endforeach
    </ul>
@endif

@if(session('error'))
    <ul class="list-group">
        <li class="list-group-item list-group-item-danger">{{session('error')}}</li>
    </ul>
@endif
